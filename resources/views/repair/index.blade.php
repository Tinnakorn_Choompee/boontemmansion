@extends('layouts.app')
@section('title', 'Repair')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                @role('member')
                <a href="{{ route('repair.create') }}" class="btn btn-primary btn-sm btn-create"> <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มข้อมูลแจ้งซ่อม </a>
                @endrole
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> ข้อมูลแจ้งซ่อม
                                @role('admin')
                                <span class="pull-right text-info" style="font-size:20px !important"> สามารถยืนยันการแจ้งซ่อมได้ที่ ปุ่ม ดูรายละเอียด </span>
                                @endrole
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr class="font text-center">
                                            <th width="5%">#</th>
                                            <th class="text-left" width="20%">หัวข้อ</th>
                                            <th width="20%">สถานะ</th>
                                            <th width="15%">วัน/เวลา ที่แจ้งซ่อม</th>
                                            <th width="15%">วัน/เวลา ดำเนินการซ่อม</th>
                                            <th width="30%">ตัวเลือก</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($repair as $k => $rs)
                                        <tr class="text-center">
                                            <td class="text-center">{{ ++$k }}</td>
                                            <td class="text-left">{{ $rs->title }}</td>
                                            <td>
                                                @switch($rs->status)
                                                    @case(0)
                                                    <span class="label label-warning font"> รอดำเนินการ </span>
                                                    @break
                                                    @case(1)
                                                    <span class="label label-info font"> รับเรื่องแล้ว </span>
                                                    @break
                                                    @case(2)
                                                    <span class="label label-success font"> ดำเนินการเสร็จสิ้น </span>
                                                    @break
                                                @endswitch
                                            </td>
                                            <td>{!! dt_th($rs->created_at) !!}</td>
                                            <td>
                                                @switch($rs->status)
                                                    @case(0)

                                                    @break
                                                    @case(1)
                                                    <b> {!! dt_th($rs->datetime_repair) !!} </b>
                                                    @break
                                                    @case(2)
                                                    <b> {!! dt_th($rs->datetime_repair) !!} </b>
                                                    @break
                                                @endswitch
                                            </td>
                                            <td class="text-center">
                                                <a class="btn btn-success" href="{{ route('repair.show', $rs->id) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                                @role('member')
                                                <a class="btn btn-warning btn-edit" href="{{ route('repair.edit', $rs->id) }}"><i class="fa fa-pencil"></i></a>
                                                @endrole
                                                @role('admin|member')
                                                <button class="btn btn-danger btn-del"  data-id="{{ $rs->id }}"><i class="fa fa-trash"></i></button>
                                                {{ Form::open(['method' => 'DELETE', 'route' => ['repair.destroy', $rs->id], 'id'=>'form-delete-'.$rs->id]) }} {{ Form::close() }}
                                                @endrole
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END TABLE HOVER -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@push('scripts')
@if (session('success'))
<script>
    swal("Success!", "บันทึกข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('update'))
<script>
    swal("Updated!", "แก้ไขข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('delete'))
<script>
    swal("Deleted!", "ลบข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('process'))
<script>
    swal("Process!", "ดำเนินการรับเรื่องเรียบร้อยแล้ว", "success");
</script>
@endif
<script>
    $('.btn-del').on('click',function(){
        var id = $(this).data('id');
        swal({
            title: 'Are you sure?',
            text: "ต้องการที่จะลบ ข้อมูลแจ้งซ่อม นี้ใช่หรือไม่ !!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
        if (result.value) {
            $( "#form-delete-"+id ).submit();
        }
        });
    });
</script>
@endpush
