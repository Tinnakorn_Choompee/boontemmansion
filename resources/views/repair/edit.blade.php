@extends('layouts.app')
@section('title', 'แก้ไขข้อมูลแจ้งซ่อม')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- INPUTS -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> แก้ไขข้อมูลแจ้งซ่อม </h3>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="panel-body">
                            {!! Form::model($repair, ['route'=>['repair.update', $repair->id], 'method'=>'patch']) !!}
                                {!! Form::label('title', 'หัวข้อ', ['class'=>'font']) !!}
                                {!! Form::text('title', NULL, ['class'=>'form-control', 'required']) !!}
                                <br>
                                {!! Form::label('detail', 'รายละเอียด', ['class'=>'font']) !!}
                                {!! Form::textarea('detail', NULL, ['class'=>'form-control', 'required']) !!}
                                <br>
                                <button type="submit" class="btn btn-primary font"> บันทึก </button>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <!-- END INPUTS -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@push('scripts')
@endpush
