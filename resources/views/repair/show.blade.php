@extends('layouts.app')
@section('title', 'ข้อมูลแจ้งซ่อม')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="panel panel-profile">
                <div class="clearfix">
                    <!-- LEFT COLUMN -->
                    <div class="profile">
                        <!-- PROFILE HEADER -->
                        <div class="profile-header">
                            <div class="overlay"></div>
                            <div class="profile-main">
                                    <p class="font" style="font-size:50px !important"> ห้อง {{ $repair->user->room->number }}</p>
                                    <p class="font" style="font-size:25px !important"> ผู้เช่า {{ $repair->user->full_name }}</p>
                                    @role('admin')
                                    <br>
                                    @switch($repair->status)
                                        @case(0)
                                        <button type="button" class="btn btn-primary font" data-toggle="modal" data-target="#model_repair" data-id="{{ $repair->id }}"> ยืนยันการแจ้งซ่อม </button>
                                        @break
                                        @case(1)
                                        <button type="button"  class="btn btn-success btn-repair font"  data-id="{{ $repair->id }}"> ยืนยันการซ่อมสำเร็จ </button>
                                        @break
                                    @endswitch
                                    @endrole
                                </div>
                        </div>
                        <!-- END PROFILE HEADER -->
                        <!-- PROFILE DETAIL -->
                        <div class="profile-detail">
                            <div class="profile-info">
                                <h6 class="heading font"> หัวข้อ </h6>
                                <p> {{ $repair->title }}</p>
                                <hr>
                                <h6 class="heading font"> รายละเอียด </h6>
                                <p> {{ $repair->detail }}</p>
                                <hr>
                                <h6 class="heading font"> วัน/เวลา ที่แจ้งซ่อม </h6>
                                <p> {!! dt_th($repair->created_at) !!} </p>
                                <hr>
                                <h6 class="heading font">
                                    <span style="margin-right:10px"> สถานะ </span>
                                    @switch($repair->status)
                                    @case(0)
                                    <span class="label label-warning font"> รอดำเนินการ </span>
                                    @break
                                    @case(1)
                                    <span class="label label-info font"> รับเรื่องแล้ว </span>
                                    @break
                                    @case(2)
                                    <span class="label label-success font"> ดำเนินการเสร็จสิ้น </span>
                                    @break
                                @endswitch
                                </h6>
                            </div>
                        </div>
                        <!-- END PROFILE DETAIL -->
                    </div>
                    <!-- END LEFT COLUMN -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->


<div class="modal fade" id="model_repair">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title font" style="font-size:20px;"> รายละเอียดการซ่อม </h5>
            </div>
            <div class="modal-body">
                {!! Form::open(['route'=>'process.repair', 'method'=>'patch']) !!}
                {!! Form::hidden('id', NULL,['class'=>'id']) !!}
                <div class="form-group">
                    <label for="datetime_repair" class="col-form-label font">วันที่ ดำเนินการซ่อม :</label>
                    <input name="date" type="text" class="form-control date" value="{{ Carbon::now()->format('Y-m-d') }}">
                </div>
                <div class="form-group">
                    <label for="datetime_repair" class="col-form-label font">เวลาที่ ดำเนินการซ่อม : </label>
                    <br>
                    <input name="time" type="text" class="form-control time" value="{{ Carbon::now()->format('H:i') }}">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default font" data-dismiss="modal">ปิดหน้าต่าง</button>
                <button type="submit" class="btn btn-primary font">บันทึก</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection

@push('styles')
{{ Html::style('vendor/flatpickr/flatpickr.min.css') }}
@endpush

@push('scripts')
{{ Html::script('vendor/flatpickr/flatpickr.js') }}
{{ Html::script('vendor/flatpickr/th.js') }}
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#model_repair').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id     = button.data('id')
        var modal  = $(this)
        modal.find('.modal-body .id').val(id)
    });

    $(".date").flatpickr({
        locale: "th",
        minDate : "today",
    });

    $(".time").flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
        defaultDate: new Date(),
        minuteIncrement : 1,
    });

    $('.btn-repair').on('click',function(){
        var id = $(this).data('id');
        swal({
            title: 'Are you sure?',
            text: "ต้องการที่จะ ยืนยันการซ่อมสำเร็จ ใช่หรือไม่ !!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#00AAFF',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PATCH",
                    url: "/success/repair",
                    data : { id:id },
                    success: function(rs){
                        swal("Success!", "ทำการยยืนยันการซ่อมสำเร็จเรียบร้อยแล้ว", "success", {button:false});
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
				    }
			    });
            }
        });
    });


</script>
@endpush
