@extends('layouts.app')
@section('title', 'Payment')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
             <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> ประวัติการชำระเงิน
                                @role('member')
                                @if($count)
                                <span class="text-danger pull-right" style="font-size:20px">** กรณีไม่อนุมัติชำระเงิน สามารถดูสาเหตุที่ไม่อนุมัติได้ที่ ปุ่มดูรายละเอียด พร้อมกับทำการแจ้งชำระเงินใหม่ </span>
                                @endif
                                @endrole
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr class="font text-center">
                                            <th width="5%">#</th>
                                            <th class="text-left" width="10%">เลขห้อง</th>
                                            <th class="text-left" width="25%">ชื่อ</th>
                                            <th width="15%">สถานะ</th>
                                            <th width="15%">วัน/เวลา อนุมัติชำระเงิน</th>
                                            <th width="10%">จำนวนเงิน</th>
                                            <th width="25%">ตัวเลือก</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($payment as $k => $rs)
                                        <tr class="text-center">
                                            <td class="text-center">{{ ++$k }}</td>
                                            <td class="text-center">{{ $rs->number }}</td>
                                            <td class="text-left">{{ $rs->user->full_name }}</td>
                                            <td>
                                                @switch($rs->status)
                                                    @case(2)
                                                    <span class='label label-success font'> ชำระเงินเรียบร้อยแล้ว </span>
                                                    @break
                                                    @case(3)
                                                    <span class='label label-danger font'> ไม่อนุมัติชำระเงิน </span>
                                                    @break
                                                @endswitch
                                            </td>
                                            <td>{!! dt_th($rs->update_at) !!}</td>
                                            <td class="text-center"> {{ number_format($rs->total) }}</td>
                                            <td class="text-center">
                                                @switch($rs->status)
                                                    @case(2)
                                                    <a class="btn btn-info" href="{{ route('history.print', $rs->id) }}"> <i class="fa fa-print" aria-hidden="true"></i> </a>
                                                    @break
                                                @endswitch
                                                <a class="btn btn-success" href="{{ route('history.show', $rs->id) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ url('/fonts/THSarabunNew.ttf') }}
                            </div>
                        </div>
                    </div>
                    <!-- END TABLE HOVER -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@push('scripts')
@if (session('approve'))
<script>
    swal("Success!", "อนุมัติการชำระเงินเรียบร้อยแล้ว", "success");
</script>
@elseif(session('not'))
<script>
    swal("Not allowed", "ไม่อนุมัติการชำระเงิน", "error");
</script>
@endif
@endpush
