@extends('layouts.app')
@section('title', 'ข้อมูลการชำระเงิน')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="panel panel-profile">
                <div class="clearfix">
                    <!-- LEFT COLUMN -->
                    <div class="profile">
                        <!-- PROFILE HEADER -->
                        <div class="profile-header">
                            <div class="overlay"></div>
                            <div class="profile-main">
                                <p class="font" style="font-size:50px !important"> ห้อง {{ $payment->number }}</p>
                                <p class="font" style="font-size:50px !important"> {{ $payment->user->full_name }}</p>
                            </div>
                        </div>
                        <!-- END PROFILE HEADER -->
                        <!-- PROFILE DETAIL -->
                        <div class="profile-detail">
                            @role('admin|member')
                                @switch($payment->status)
                                @case(3)
                                <div class="profile-info">
                                    <h5 class="heading font text-danger"> สาเหตุที่ไม่อนุมัติการชำระ </h5>
                                    <hr>
                                    <p class="text-danger font"> {{ $payment->detail->note }}
                                    @role('member')
                                    <button type="button" class="btn btn-success font btn-lg pull-right" data-toggle="modal" data-target="#myModalEdit" data-id="{{ $payment->id }}" data-total="{{ $payment->total }}">
                                            แจ้งชำระเงินใหม่
                                    </button>
                                    @endrole
                                    </p>
                                </div>
                                @break
                                @endswitch
                            @endrole
                            @role('admin|member')
                            <div class="profile-info">
                                <h5 class="heading font"> รายละเอียดการชำระเงิน </h5>
                                <hr>
                                <ul class="list-unstyled list-justify">
                                    <li class="font"> วัน <span> {{ Carbon::parse($payment->detail->date)->format('d/m/Y') }} </span></li>
                                    <br>
                                    <li class="font"> เวลา <span> {{ Carbon::parse($payment->detail->time)->format('H:i') }} </span></li>
                                    <br>
                                    <li class="font"> รวมทั้งสิ้น <span> {{ number_format($payment->detail->total) }} </span></li>
                                    <br>
                                    @isset($payment->detail->image)
                                    <li class="font"> หลักฐานการชำระเงิน <span>
                                        <a data-fancybox="gallery" href="{{asset('image/payment/'.$payment->detail->image)}}">
                                         {!! Html::image('image/payment/'.$payment->detail->image, NULL, ['class'=>'img-rounded', 'width'=>'100']) !!}
                                        </a>
                                        </span></li>

                                    @endisset
                                </ul>
                            </div>
                            @endrole
                            <div class="profile-info">
                                <h5 class="heading font"> รายละเอียด </h5>
                                <hr>
                                <ul class="list-unstyled list-justify">
                                    <li class="font"> ประเภทห้อง <span> {{ $payment->type }} </span></li>
                                    <br>
                                    <li class="font"> ค่าเช่า <span> {{ number_format($payment->amount) }} </span></li>
                                    <br>
                                    <li class="font"> ค่าน้ำ <span> {{ number_format($payment->water) }} </span></li>
                                    <br>
                                    <li class="font"> มิเตอร์เริ่มต้น <span> {{ $payment->meter_before }} </span></li>
                                    <br>
                                    <li class="font"> มิเตอร์สิ้นสุด <span> {{ $payment->meter_after }} </span></li>
                                    <br>
                                    <li class="font"> จำนวนหน่วย <span> {{ $payment->unit }} </span></li>
                                    <br>
                                    <li class="font"> ค่าไฟฟ้า <span> {{ number_format($payment->electricity) }} </span></li>
                                    <br>
                                    <li class="font"> รวมทั้งสิ้น <span> {{ number_format($payment->total) }} </span></li>
                                    <br>
                                    <li class="font"> สถานะ
                                        @switch($payment->status)
                                            @case(2)
                                            <span class='label label-success font'> ชำระเงินเรียบร้อยแล้ว </span>
                                            @break
                                            @case(3)
                                            <span class='label label-danger font'> ไม่อนุมัติชำระเงิน </span>
                                            @break
                                        @endswitch
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- END PROFILE DETAIL -->
                    </div>
                    <!-- END LEFT COLUMN -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!-- Modal -->
<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top:5%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font">แจ้งชำระเงิน</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:0%;padding-left:20%;">
            <span aria-hidden="true">&times;</span>
          </button>
            </div>
            {{ Form::open(['route'=>'notification.payment', 'files'=> TRUE]) }}
            {{ Form::hidden('payment_id', NULL, ['class'=>'payment_id']) }}
            <div class="modal-body">
                <div class="card-body">
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('date', 'วันที่โอนเงิน', ['class'=>'font']) !!}
                                    {!! Form::text('date', NULL, ['class'=>'form-control date', 'required']) !!}
                                    @if ($errors->has('date'))
                                    <small class="form-control-feedback text-danger"> {{ $errors->first('date') }} </small>
                                    @endif
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('time', 'เวลาที่โอนเงิน', ['class'=>'font']) !!}
                                    {!! Form::text('time', NULL, ['class'=>'form-control time', 'required']) !!}
                                    @if ($errors->has('time'))
                                    <small class="form-control-feedback text-danger"> {{ $errors->first('time') }} </small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('total', 'ยอดเงิน', ['class'=>'font']) !!}
                                    {!! Form::number('total', NULL, ['class'=>'form-control total', 'required']) !!}
                                    @if ($errors->has('total'))
                                    <small class="form-control-feedback text-danger"> {{ $errors->first('total') }} </small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('image', 'หลักฐานการชำระเงิน', ['class'=>'font']) !!}
                                    {!! Form::file('image', ['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary font" data-dismiss="modal">ยกเลิก</button>
                <button type="submit" class="btn btn-primary font">แจ้งชำระเงิน</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!-- Modal -->

<!-- Modal Edit-->
<div class="modal fade" id="myModalEdit"  role="dialog" aria-labelledby="myModalEditLabel">
    <div class="modal-dialog" role="document" style="margin-top:5%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font">แจ้งชำระเงินใหม่</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:0%;padding-left:20%;">
            <span aria-hidden="true">&times;</span>
          </button>
            </div>
            {{ Form::open(['route'=>'notification.edit', 'files'=> TRUE]) }}
            {{ Form::hidden('payment_id', NULL, ['class'=>'payment_id']) }}
            <div class="modal-body">
                <div class="card-body">
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('date', 'วันที่โอนเงิน', ['class'=>'font']) !!}
                                    {!! Form::text('date', NULL, ['class'=>'form-control date', 'required']) !!}
                                    @if ($errors->has('date'))
                                    <small class="form-control-feedback text-danger"> {{ $errors->first('date') }} </small>
                                    @endif
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('time', 'เวลาที่โอนเงิน', ['class'=>'font']) !!}
                                    {!! Form::text('time', NULL, ['class'=>'form-control time', 'required']) !!}
                                    @if ($errors->has('time'))
                                    <small class="form-control-feedback text-danger"> {{ $errors->first('time') }} </small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('total', 'ยอดเงิน', ['class'=>'font']) !!}
                                    {!! Form::number('total', NULL, ['class'=>'form-control total', 'required']) !!}
                                    @if ($errors->has('total'))
                                    <small class="form-control-feedback text-danger"> {{ $errors->first('total') }} </small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('image', 'หลักฐานการชำระเงิน', ['class'=>'font']) !!}
                                    {!! Form::file('image', ['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary font" data-dismiss="modal">ยกเลิก</button>
                <button type="submit" class="btn btn-primary font">แจ้งชำระเงิน</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<!-- Modal -->

{{ Form::open(['route'=>['payment.approve', $payment->id], 'id'=>'approve-form']) }}
{{ Form::hidden('approve', NULL, ['id'=>'approve']) }}
{{ Form::hidden('note', NULL, ['id'=>'note']) }}
{{ Form::close() }}

@endsection
@push('styles')
{{ Html::style('vendor/flatpickr/flatpickr.min.css') }}
{{ Html::style('vendor/fancybox/jquery.fancybox.min.css') }}
@endpush
@push('scripts')
{{ Html::script('vendor/fancybox/jquery.fancybox.min.js') }}
{{ Html::script('vendor/flatpickr/flatpickr.js') }}
{{ Html::script('vendor/flatpickr/th.js') }}
@if (session('payment'))
<script>
    swal("Payment!", "แจ้งชำระเงินเรียบร้อยแล้ว", "success");
</script>
@endif
<script>
    $('#myModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var total = button.data('total')
        var id    = button.data('id')

        $('.total').val(total);
        $('.payment_id').val(id);

        $(".date").flatpickr({
            locale: "th",
            minDate : "today",
            defaultDate: new Date(),
        });
        $(".time").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            time_24hr: true,
            defaultDate: new Date(),
        });
    });

    $('#myModalEdit').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var total = button.data('total')
        var id    = button.data('id')

        $('.total').val(total);
        $('.payment_id').val(id);

        $(".date").flatpickr({
            locale: "th",
            minDate : "today",
            defaultDate: new Date(),
        });
        $(".time").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            time_24hr: true,
            defaultDate: new Date(),
        });
    });

    $('.btn-approve').on('click',function(e){
        e.preventDefault();
        swal({
            title: 'Are you sure?',
            text: "ต้องการที่อนุมัติ การชำระเงิน นี้ใช่หรือไม่ !!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                $('#approve').val(1);
                $("#approve-form").submit();
            }
        });
    });

    $('.btn-not').on('click',function(e){
        e.preventDefault();
        swal("สาเหตุที่ไม่อนุมัติการชำระ", {
            content: "input",
        })
        .then((value) => {
            $('#approve').val(0);
            $('#note').val(value);
            $("#approve-form").submit();
        });
    });
</script>
@endpush
