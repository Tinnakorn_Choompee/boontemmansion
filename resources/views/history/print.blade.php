<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title> ใบเสร็จรับเงิน </title>
    <style>
      @font-face {
          font-family: 'THSarabunNew';
          font-style: normal;
          font-weight: normal;
          src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
      }
      @font-face {
          font-family: 'THSarabunNew';
          font-style: normal;
          font-weight: bold;
          src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
      }
      @font-face {
          font-family: 'THSarabunNew';
          font-style: italic;
          font-weight: normal;
          src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
      }
      @font-face {
          font-family: 'THSarabunNew';
          font-style: italic;
          font-weight: bold;
          src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
      }

      body { font-family: "THSarabunNew" }

      .absolute {
        position: absolute;
        text-align: right;
        vertical-align: middle;
      }

      table.separate {
        border-collapse: separate;
        width: 100%;
      }

      table.collapse {
        border-collapse: collapse;
        border: 1px solid black;
      }

      table.collapse td {
        padding-bottom : 10px;
      }

      table.collapse th {
        border: 1px solid black
      }

      .full-width {
        width: 100%;
      }

      th {
        text-align: center;
        vertical-align: middle;
      }

      td,th {
        vertical-align: middle;
      }
      .bold {
          font-weight : bold;
      }

      .border {
        border-left: 1px solid black;
      }

      .center {
        text-align: center;
      }

      .right {
        text-align: right;
      }

      .left {
        text-align: left;
      }

      .m_left {
        margin-left : 10px
      }

      .m_right {
        margin-right : 10px
      }

      .f_right {
        float : right;
      }

      .f_left {
        float : left;
      }
      @page { size: 21.0cm 20cm; }
    </style>
  </head>

  <body marginwidth="0" marginheight="0">

    <h1> ใบเสร็จรับเงิน (Receipt) <span class="f_right"> หอพักบุญเต็มแมนชั่น </span> </h1>
    <h3> <span class="m_right"> โทรศัพท์ 053-486452 </span> เบอร์โทร 080-1512325 <span class="f_right"> 351/4 ต.ดอนแก้ว อ.แม่ริม จ.เชียงใหม่ 50180 </span></h3>

    <hr>
    <hr>

    <table class="separate">
      <tbody>
        <tr>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> ชื่อผู้เช่า </span> </td>
          <td> <span style="font-size:20px;"> {{ $history->user->fullname }} </span> </td>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> เลขห้อง </span> </td>
          <td> <span style="font-size:20px;"> {{ $history->number }} </span> </td>
        </tr>
        <tr>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> ประจำเดือน </span> </td>
          <td> <span style="font-size:20px;"> {{ month($history->created_at)  }} </span> </td>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> วันที่ชำระเงิน </span> </td>
          <td>
            <span style="font-size:20px;">
                    {{ datetime_th($history->detail->date, $history->detail->time) }}
            </span>
          </td>
        </tr>
      </tbody>
    </table>

    <hr>
    <hr>

    <br>
    <table class="collapse full-width">
      <thead>
        <tr>
          <th width="35%"> รายการ </th>
          <th width="35%"> หน่วย </th>
          <th width="30%"> ราคาต่อหน่วย </th>
          <th width="30%"> จำนวนเงิน </th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <td class="border">
                <span class="m_left"> ค่าเช่าห้องพัก </span>
            </td>
            <td class="border center">
                <span class="m_left">  1 </span>
            </td>
            <td class="border right">
                <span class="m_right"> {{ number_format($history->amount,2,'.',',') }} </span>
            </td>
            <td class="border right">
                <span class="m_right"> {{ number_format($history->amount,2,'.',',') }} </span>
            </td>
        </tr>
        <tr>
            <td class="border">
                <span class="m_left"> ค่าน้ำ </span>
            </td>
            <td class="border center">
                <span class="m_left">  1 </span>
            </td>
            <td class="border right">
                <span class="m_right"> {{ number_format($history->water,2,'.',',') }} </span>
            </td>
            <td class="border right">
                <span class="m_right"> {{ number_format($history->water,2,'.',',') }} </span>
            </td>
        </tr>
         <tr>
            <td class="border">
                <span class="m_left"> ค่าไฟฟ้า ( มิเตอร์ {{ $history->meter_before }} ถึง  {{ $history->meter_after }} ) </span>
            </td>
            <td class="border center">
                <span class="m_left">  {{ $history->unit }} </span>
            </td>
            <td class="border right">
                <span class="m_right"> 7 </span>
            </td>
            <td class="border right">
                <span class="m_right"> {{ number_format($history->electricity,2,'.',',') }} </span>
            </td>
        </tr>
      </tbody>
    </table>
    <br>
    <hr>
    <hr>

    <p class="f_right bold" style="font-size: 20px"> รวมเงินทั้งสิ้น  {{ number_format($history->total, 2,'.', ',') }} บาท ({{ Numberthai($history->total) }}) </p>
    <br>
    <br>
    <br>
    <hr>
    <hr>
  </body>
</html>
