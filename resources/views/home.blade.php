@extends('layouts.app')
@section('title', 'Home | Boontemmansion')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        @role('owner|admin')
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title"> หน้าหลัก </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="metric">
                                <span class="icon"><i class="lnr lnr-users"></i></span>
                                <p>
                                    <span class="number"> {{ $member }} </span>
                                    <span class="title"> จำนวนผู้เช่า </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="metric">
                                <span class="icon"><i class="lnr lnr-apartment"></i></span>
                                <p>
                                    <span class="number"> {{ $room }} </span>
                                    <span class="title"> ห้องว่าง </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
        @endrole
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title"> เมนู </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        @foreach(menu() as $k => $v) @role($v['role'])
                                        <div class="col-sm-6 col-md-3 animated fadeInRight">
                                            <div class="block text-center">
                                                <a href="{{ $v['route'] }}" class="hvr-pop">
                                                    <div class="icon">
                                                        <br>
                                                        <i class="lnr {{ $v['icon'] }}" style="font-size:30px"></i>
                                                        <br>
                                                        <span class="font" style="font-size:20px"> {{ $v['name'] }}
                                                        </span>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        @endrole @endforeach
                                        <div class="col-sm-6 col-md-3 animated fadeInRight">
                                            <div class="block text-center">
                                                <a href="{{ route('profile.index') }}" class="hvr-pop">
                                                    <div class="icon">
                                                        <br>
                                                        <i class="lnr lnr-user" style="font-size:30px"></i>
                                                        <br>
                                                        <span class="font" style="font-size:20px"> โปรไฟล์ </span>
                                                        <br>
                                                        <br>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-3 animated fadeInRight">
                                            <div class="block text-center">
                                                <a href="{{ route('logout') }}" class="hvr-pop"
                                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                                    <div class="icon">
                                                        <br>
                                                        <i class="lnr lnr-exit" style="font-size:30px"></i>
                                                        <br>
                                                        <span class="font" style="font-size:20px"> ออกจากระบบ </span>
                                                        {!!
                                                        Form::open(['url' => 'logout', 'id'=>'logout-form']) !!} {!!
                                                        Form::close()
                                                        !!}
                                                        <br>
                                                        <br>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- ./box-body -->

                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="font"> หอพักบุญเต็มแมนชั่น </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
        @role('owner')
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-headline">
                        <div class="panel-heading text-center">
                            <h3 class="panel-title"> กราฟ รายได้หอพัก ประจำปี {{ Carbon::now()->year +543 }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <div style="margin:auto;width:90%;">
                                            <canvas id="ChartYear" width="450" height="350"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-headline">
                        <div class="panel-heading text-center">
                            <h3 class="panel-title"> กราฟ รวมยูนิตไฟฟ้า / ต่อหน่วย ประจำปี
                                {{ Carbon::now()->year + 543 }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <div style="margin:auto;width:90%;">
                                            <canvas id="ChartUnit" width="450" height="350"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-headline">
                        <div class="panel-heading text-center">
                            <h3 class="panel-title"> กราฟ แจ้งซ่อม ประจำปี {{ Carbon::now()->year + 543 }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <div style="margin:auto;width:80%;">
                                            <canvas id="ChartRepair" width="450" height="350"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-headline">
                        <div class="panel-heading text-center">
                            <h3 class="panel-title"> กราฟ แจ้งเปลี่ยนแปลงห้อง ประจำปี {{ Carbon::now()->year + 543 }}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body">
                                        <div style="margin:auto;width:80%;">
                                            <canvas id="ChartChange" width="450" height="350"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
        @endrole
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection

@role('owner')
@push('scripts')
{{ Html::script('vendor/chartjs/Chart.min.js') }}
<!----------------------------------------------------------  ChartSetting  -------------------------------------------------------------->
<script>
    // Global Options:
	Chart.defaults.global.defaultFontColor = 'black';
	Chart.defaults.global.defaultFontSize = 22;
	Chart.defaults.global.defaultFontFamily = 'DBHelvethaica';
</script>
<!----------------------------------------------------------  ChartConduct  -------------------------------------------------------------->
<script>
    var ChartYear = document.getElementById("ChartYear").getContext('2d');
        var month     = new Chart(ChartYear, {
          type: 'bar',
          data: {
            labels: [
                @foreach(M() as $rs)
                    '{{ $rs }}',
                @endforeach
            ],
            datasets: [{
                label: false,
                data: [
					@foreach($payment as $rs)
						'{{ $rs }}',
					@endforeach
				],
				backgroundColor: [
					@foreach(Color() as $rs)
						'{{ $rs }}',
					@endforeach
				],
				borderColor: [
					@foreach(Border() as $rs)
						'{{ $rs }}',
					@endforeach
				],
				borderWidth: 1,
            }]
          },
          options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                }
                }
            },
            animation: {
                onComplete: function () {
                    var ctx = this.chart.ctx;
                    ctx.textAlign = "center";
                    ctx.textBaseline = "middle";
                    var chart    = this;
                    var datasets = this.config.data.datasets;
                    datasets.forEach(function (dataset, i) {
                        ctx.font = "22px DBHelvethaica";
                        switch (dataset.type) {
                            case "lines":
                                ctx.fillStyle = "Black";
                                chart.getDatasetMeta(i).data.forEach(function (p, j) {
                                    ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 20);
                                });
                                break;
                            case "bars":
                                ctx.fillStyle = "Black";
                                chart.getDatasetMeta(i).data.forEach(function (p, j) {
                                    if(datasets[i].data[j] != 0) { ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 18); }
                                });
                                break;
                        }
                    });
                }
            },
            scales: {
              responsive: true,
              yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero: true,
                    steps: 10,
                    stepValue: 5,
                    max : {{ max($payment) + 250 }}
                }
              }],
              xAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                  fontSize : 22,
                  fontFamily : 'DBHelvethaica',
                  fontStyle : 'normal',
                }
              }]
            }
          }
        });

        var ChartUnit = document.getElementById("ChartUnit").getContext('2d');
        var unit      = new Chart(ChartUnit, {
          type: 'bar',
          data: {
            labels: [
                @foreach(M() as $rs)
                    '{{ $rs }}',
                @endforeach
            ],
            datasets: [{
                label: false,
                data: [
					@foreach($unit as $rs)
						'{{ $rs }}',
					@endforeach
				],
				backgroundColor: [
					@foreach(Color() as $rs)
						'{{ $rs }}',
					@endforeach
				],
				borderColor: [
					@foreach(Border() as $rs)
						'{{ $rs }}',
					@endforeach
				],
				borderWidth: 1,
            }]
          },
          options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                }
                }
            },
            animation: {
                onComplete: function () {
                    var ctx = this.chart.ctx;
                    ctx.textAlign = "center";
                    ctx.textBaseline = "middle";
                    var chart    = this;
                    var datasets = this.config.data.datasets;
                    datasets.forEach(function (dataset, i) {
                        ctx.font = "22px DBHelvethaica";
                        switch (dataset.type) {
                            case "lines":
                                ctx.fillStyle = "Black";
                                chart.getDatasetMeta(i).data.forEach(function (p, j) {
                                    ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 20);
                                });
                                break;
                            case "bars":
                                ctx.fillStyle = "Black";
                                chart.getDatasetMeta(i).data.forEach(function (p, j) {
                                    if(datasets[i].data[j] != 0) { ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 18); }
                                });
                                break;
                        }
                    });
                }
            },
            scales: {
              responsive: true,
              yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero: true,
                    steps: 10,
                    stepValue: 5,
                    max : {{ max($unit) + 2 }}
                }
              }],
              xAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                  fontSize : 22,
                  fontFamily : 'DBHelvethaica',
                  fontStyle : 'normal',
                }
              }]
            }
          }
        });

        var ChartRepair = document.getElementById("ChartRepair").getContext('2d');
        var repair      = new Chart(ChartRepair, {
          type: 'bar',
          data: {
            labels: [
                @foreach(M() as $rs)
                    '{{ $rs }}',
                @endforeach
            ],
            datasets: [{
                label: false,
                data: [
					@foreach($repair as $rs)
						'{{ $rs }}',
					@endforeach
				],
				backgroundColor: [
					@foreach(Color() as $rs)
						'{{ $rs }}',
					@endforeach
				],
				borderColor: [
					@foreach(Border() as $rs)
						'{{ $rs }}',
					@endforeach
				],
				borderWidth: 1,
            }]
          },
          options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                }
                }
            },
            animation: {
                onComplete: function () {
                    var ctx = this.chart.ctx;
                    ctx.textAlign = "center";
                    ctx.textBaseline = "middle";
                    var chart    = this;
                    var datasets = this.config.data.datasets;
                    datasets.forEach(function (dataset, i) {
                        ctx.font = "22px DBHelvethaica";
                        switch (dataset.type) {
                            case "lines":
                                ctx.fillStyle = "Black";
                                chart.getDatasetMeta(i).data.forEach(function (p, j) {
                                    ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 20);
                                });
                                break;
                            case "bars":
                                ctx.fillStyle = "Black";
                                chart.getDatasetMeta(i).data.forEach(function (p, j) {
                                    if(datasets[i].data[j] != 0) { ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 18); }
                                });
                                break;
                        }
                    });
                }
            },
            scales: {
              responsive: true,
              yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero: true,
                    steps: 10,
                    stepValue: 5,
                    max : {{ max($repair) + 2 }}
                }
              }],
              xAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                  fontSize : 22,
                  fontFamily : 'DBHelvethaica',
                  fontStyle : 'normal',
                }
              }]
            }
          }
        });

        var ChartChange = document.getElementById("ChartChange").getContext('2d');
        var change      = new Chart(ChartChange, {
          type: 'bar',
          data: {
            labels: [
                @foreach(M() as $rs)
                    '{{ $rs }}',
                @endforeach
            ],
            datasets: [{
                label: false,
                data: [
					@foreach($change as $rs)
						'{{ $rs }}',
					@endforeach
				],
				backgroundColor: [
					@foreach(Color() as $rs)
						'{{ $rs }}',
					@endforeach
				],
				borderColor: [
					@foreach(Border() as $rs)
						'{{ $rs }}',
					@endforeach
				],
				borderWidth: 1,
            }]
          },
          options: {
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                }
                }
            },
            animation: {
                onComplete: function () {
                    var ctx = this.chart.ctx;
                    ctx.textAlign = "center";
                    ctx.textBaseline = "middle";
                    var chart    = this;
                    var datasets = this.config.data.datasets;
                    datasets.forEach(function (dataset, i) {
                        ctx.font = "22px DBHelvethaica";
                        switch (dataset.type) {
                            case "lines":
                                ctx.fillStyle = "Black";
                                chart.getDatasetMeta(i).data.forEach(function (p, j) {
                                    ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 20);
                                });
                                break;
                            case "bars":
                                ctx.fillStyle = "Black";
                                chart.getDatasetMeta(i).data.forEach(function (p, j) {
                                    if(datasets[i].data[j] != 0) { ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 18); }
                                });
                                break;
                        }
                    });
                }
            },
            scales: {
              responsive: true,
              yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero: true,
                    steps: 10,
                    stepValue: 5,
                    max : {{ max($repair) + 2 }}
                }
              }],
              xAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                  fontSize : 22,
                  fontFamily : 'DBHelvethaica',
                  fontStyle : 'normal',
                }
              }]
            }
          }
        });
</script>
@endpush
@endrole