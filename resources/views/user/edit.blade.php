@extends('layouts.app')
@section('title', 'แก้ไขผู้ใช้งานระบบ')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- INPUTS -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> แก้ไขผู้ใช้งานระบบ </h3>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="panel-body">
                            {!! Form::model($user, ['route'=> ['user.update', $user->id], 'method'=>'PATCH']) !!}
                                {!! Form::label('prefix', 'คำนำหน้า', ['class'=>'font']) !!}
                                {!! Form::select('prefix', ['นาย'=> 'นาย', 'นาง'=>'นาง'], NULL,['class'=>'form-control', 'required']) !!}
                                <br>
                                {!! Form::label('First_name', 'ชื่อ', ['class'=>'font']) !!}
                                {!! Form::text('first_name', NULL, ['class'=>'form-control', 'placeholder'=>'First Name', 'required']) !!}
                                <br>
                                {!! Form::label('Last_name', 'นามสกุล', ['class'=>'font']) !!}
                                {!! Form::text('last_name', NULL, ['class'=>'form-control', 'placeholder'=>'Last Name', 'required']) !!}
                                <br>
                                {!! Form::label('Email', 'อีเมล์', ['class'=>'font']) !!}
                                {!! Form::email('email', NULL, ['class'=>'form-control', 'placeholder'=>'Email', 'required']) !!}
                                <br>
                                {!! Form::label('Phone', 'เบอร์โทร', ['class'=>'font']) !!}
                                {!! Form::tel('phone', NULL, ['class'=>'form-control phone', 'placeholder'=>'Tel.', 'required' , 'maxlength'=> 10]) !!}
                                <br>
                                {!! Form::label('Username', 'ชื่อเข้าใช้ระบบ', ['class'=>'font']) !!}
                                {!! Form::text('username', NULL, ['class'=>'form-control', 'placeholder'=>'Username', 'required']) !!}
                                <br>
                                {!! Form::label('Password', 'รหัสผ่าน', ['class'=>'font']) !!}
                                {!! Form::password('password', ['class'=>'form-control', 'placeholder'=> 'Password']) !!}
                                <br>
                                <button class="btn btn-primary font" type="submit"> บันทึก </button>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <!-- END INPUTS -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@push('scripts')
<script>
    $(".phone").keydown(function(e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        ((e.keyCode == 65 || e.keyCode == 86 || e.keyCode == 67) && (e.ctrlKey === true || e.metaKey === true)) ||(e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
@endpush
