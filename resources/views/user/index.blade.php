@extends('layouts.app')
@section('title', 'User')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                @role('owner')
                <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm btn-create"> <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มผู้ใช้งาน </a>
                @endrole
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> ข้อมูลผู้ใช้งาน </h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr class="font">
                                            <th width="5%">#</th>
                                            <th width="20%">ชื่อ</th>
                                            <th width="20%">ชื่อเข้าใช้ระบบ</th>
                                            <th width="20%">อีเมล์</th>
                                            <th width="20%">เบอร์โทร</th>
                                            <th width="25%">ตัวเลือก</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $k => $user)
                                        <tr class="text-center">
                                            <td class="text-center">{{ ++$k }}</td>
                                            <td>{{ $user->full_name }}</td>
                                            <td>{{ $user->username }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td class="text-center">
                                                {{-- <a class="btn btn-success" href="{{ route('user.show', $user->id) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a> --}}
                                                <a class="btn btn-warning btn-edit" href="{{ route('user.edit', $user->id) }}"><i class="fa fa-pencil"></i></a>
                                                <button class="btn btn-danger btn-del"  data-id="{{ $user->id }}"><i class="fa fa-trash"></i></button>
                                                {{ Form::open(['method' => 'DELETE', 'route' => ['user.destroy', $user->id], 'id'=>'form-delete-'.$user->id]) }} {{ Form::close() }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END TABLE HOVER -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@push('scripts')
@if (session('success'))
<script>
    swal("Success!", "บันทึกข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('update'))
<script>
    swal("Updated!", "แก้ไขข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('delete'))
<script>
    swal("Deleted!", "ลบข้อมูลเรียบร้อยแล้ว", "success");
</script>
@endif
<script>
 $('.btn-del').on('click',function(){
    var id = $(this).data('id');
    swal({
        title: 'Are you sure?',
        text: "ต้องการที่จะลบ ผู้ใช้งาน นี้ใช่หรือไม่ !!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
    if (result.value) {
        $( "#form-delete-"+id ).submit();
    }
    });
 });
</script>
@endpush