@extends('layouts.app')
@section('title', 'ข้อมูลห้องพัก')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="panel panel-profile">
                <div class="clearfix">
                    <!-- LEFT COLUMN -->
                    <div class="profile">
                        <!-- PROFILE HEADER -->
                        <div class="profile-header">
                            <div class="overlay"></div>
                            <div class="profile-main">
                                <p class="font" style="font-size:50px !important"> ห้อง {{ $room->number }}</p>
                            </div>
                        </div>
                        <!-- END PROFILE HEADER -->
                        <!-- PROFILE DETAIL -->
                        <div class="profile-detail">
                            <div class="profile-info">
                                <h5 class="heading font"> รายละเอียด </h5>
                                <ul class="list-unstyled list-justify">
                                    <li class="font"> ประเภทห้อง
                                        <span>
                                            @switch($room->type)
                                                @case("air")
                                                แอร์
                                                @break
                                                @case("fan")
                                                พัดลม
                                                @break
                                            @endswitch
                                        </span>
                                    </li>
                                    <li class="font"> ค่าเช่า <span> {{ $room->amount }} </span></li>
                                    <li class="font"> ค่าน้ำ <span> {{ $room->water }} </span></li>
                                    <li class="font"> สถานะ <span> {!! $room->user_id ? "<span class='label label-danger font'> ไม่ว่าง </span>" : "<span class='label label-success font'> ว่าง </span>" !!} </span></li>
                                    @isset($room->user_id)
                                        <li class="font"> ผู้เช่า <span> {{ $room->user->fullname }}  </span></li>
                                    @endisset
                                </ul>
                            </div>
                        </div>
                        <!-- END PROFILE DETAIL -->
                    </div>
                    <!-- END LEFT COLUMN -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection

