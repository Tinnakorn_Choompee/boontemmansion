@extends('layouts.app')
@section('title', 'เพิ่มห้องพัก')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- INPUTS -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> เพิ่มห้องพัก </h3>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="panel-body">
                            {!! Form::open(['route'=>'room.store']) !!}
                                {!! Form::label('number', 'เลขห้อง', ['class'=>'font']) !!}
                                {!! Form::text('number', NULL, ['class'=>'form-control', 'placeholder'=>'เลขห้อง', 'required']) !!}
                                <br>
                                {!! Form::label('type', 'ประเภทห้อง', ['class'=>'font']) !!}
                                {!! Form::select('type', ['air'=> 'แอร์', 'fan'=> 'พัดลม'], NULL, ['class'=>'form-control type', 'required']) !!}
                                <br>
                                {!! Form::label('amount', 'ค่าห้อง', ['class'=>'font']) !!}
                                {!! Form::number('amount', 3500, ['class'=>'form-control amount', 'readonly']) !!}
                                <br>
                                {!! Form::label('water', 'ค่าน้ำ', ['class'=>'font']) !!}
                                {!! Form::number('water', 120, ['class'=>'form-control', 'readonly']) !!}
                                <br>
                                <button class="btn btn-primary font" type="submit"> บันทึก </button>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <!-- END INPUTS -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@push('scripts')
<script>
$('.type').on('change', function() {
    switch (this.value) {
        case "air" : $('.amount').val(3500); break;
        case "fan" : $('.amount').val(3000); break;
    }
});
</script>
@endpush
