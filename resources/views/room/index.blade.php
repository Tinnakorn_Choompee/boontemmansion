@extends('layouts.app')
@section('title', 'Room')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{-- @role('admin')
                <a href="{{ route('room.create') }}" class="btn btn-primary btn-sm btn-create"> <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มห้องพัก </a>
                @endrole --}}
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> ข้อมูลห้องพัก </h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover" id="table-data">
                                    <thead>
                                        <tr class="font">
                                            <th width="20%">เลขห้อง</th>
                                            <th width="20%">ประเภท</th>
                                            <th width="10%">ค่าเช่า</th>
                                            <th width="10%">ค่าน้ำ</th>
                                            <th width="20%">สถานะ</th>
                                            <th width="25%">รายละเอียดห้อง</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($rooms as $k => $room)
                                        <tr class="text-center">
                                            <td>{{ $room->number }}</td>
                                            <td>
                                                @switch($room->type)
                                                    @case("air")
                                                    แอร์
                                                    @break
                                                    @case("fan")
                                                    พัดลม
                                                    @break
                                                @endswitch
                                            </td>
                                            <td>{{ number_format($room->amount, 0) }}</td>
                                            <td>{{ number_format($room->water, 0) }}</td>
                                            <td>
                                                <p style="margin:10px 10px">
                                                    {!! $room->user_id ? "<span class='label label-danger font'> ไม่ว่าง </span>" : "<span class='label label-success font'> ว่าง </span>" !!}
                                                </p>
                                            </td>
                                            <td class="text-center">
                                                <a class="btn btn-info" href="{{ route('room.show', $room->id) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                                @role('admin')
                                                <a class="btn btn-warning btn-edit" href="{{ route('room.edit', $room->id) }}"><i class="fa fa-pencil"></i></a>
                                                @endrole
                                                {{-- <button class="btn btn-danger btn-del"  data-id="{{ $room->id }}"><i class="fa fa-trash"></i></button>
                                                {{ Form::open(['method' => 'DELETE', 'route' => ['room.destroy', $room->id], 'id'=>'form-delete-'.$room->id]) }} {{ Form::close() }} --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END TABLE HOVER -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@push('styles')
    <!-- DataTables -->
    {{ Html::style('vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
@endpush
@push('scripts')
   <!-- DataTables -->
   {{ Html::script('vendor/datatables.net/js/jquery.dataTables.js') }}
   {{ Html::script('vendor/datatables.net-bs/js/dataTables.bootstrap.min.js')}}
    @if (session('success'))
    <script>
        swal("Success!", "บันทึกข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @elseif (session('update'))
    <script>
        swal("Updated!", "แก้ไขข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @elseif (session('delete'))
    <script>
        swal("Deleted!", "ลบข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @endif
    <script>
         $('#table-data').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
        $('.btn-del').on('click',function(){
            var id = $(this).data('id');
            swal({
                title: 'Are you sure?',
                text: "ต้องการที่จะลบ ข้อมูลห้องพัก นี้ใช่หรือไม่ !!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
            if (result.value) {
                $( "#form-delete-"+id ).submit();
            }
            });
        });
    </script>
@endpush
