@extends('layouts.app')
@section('title', 'เพิ่มข้อมูลการชำระเงิน')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- INPUTS -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> เพิ่มข้อมูลการชำระเงิน</h3>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="panel-body">
                            {!! Form::open(['route'=>'payment.store']) !!}
                                {!! Form::label('number', 'เลือกห้อง', ['class'=>'font']) !!}
                                <select name="number" class="form-control room" required>
                                    <option value="" selected disabled> -- เลือกห้อง -- </option>
                                    @foreach ($room as $v)
                                    <option value="{{ $v }}"> {{ $v }} </option>
                                    @endforeach
                                </select>
                                {{-- {!! Form::select('room', $room, NULL, ['class'=>'', 'required']) !!} --}}
                                <br>
                                {!! Form::label('type', 'ประเภทห้อง', ['class'=>'font']) !!}
                                {!! Form::text('type', NULL, ['class'=>'form-control type', 'readonly']) !!}
                                <br>
                                {!! Form::label('amount', 'ค่าเช่า', ['class'=>'font']) !!}
                                {!! Form::number('amount', NULL, ['class'=>'form-control amount', 'readonly']) !!}
                                <br>
                                {!! Form::label('water', 'ค่าน้ำ', ['class'=>'font']) !!}
                                {!! Form::number('water', NULL, ['class'=>'form-control water', 'readonly']) !!}
                                <br>
                                {!! Form::label('meter_before', 'มิเตอร์เริ่มต้น', ['class'=>'font']) !!}
                                {!! Form::number('meter_before', NULL, ['class'=>'form-control meter_before', 'readonly']) !!}
                                <br>
                                {!! Form::label('meter_after', 'มิเตอร์สิ้นสุด', ['class'=>'font']) !!}
                                {!! Form::number('meter_after', NULL, ['class'=>'form-control meter_after', 'required']) !!}
                                <br>
                                {!! Form::label('electricity', 'ค่าไฟฟ้า', ['class'=>'font']) !!} <span class="text-danger font">  หมายเหตุ * ค่าไฟฟ้าหน่วยละ 7 บาท </span>
                                {!! Form::number('electricity', NULL, ['class'=>'form-control electricity', 'readonly']) !!}
                                <br>
                                {!! Form::label('total', 'รวมทั้งสิ้น', ['class'=>'font']) !!}
                                {!! Form::number('total', NULL, ['class'=>'form-control total', 'readonly']) !!}
                                <br>
                                <button type="submit" class="btn btn-primary font"> บันทึก </button>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <!-- END INPUTS -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection

@push('scripts')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.room').on('change', function() {
        $.ajax({
            type: "POST",
            url: "{{ route('select.payment') }}",
            data: {room: $(this).val() },
            success: function(rs){
                switch (rs.type) {
                    case "air":
                        var type = "แอร์";
                        break;
                    case "fan":
                        var type = "พัดลม";
                    break;
                }
                $('.type').val(type);
                $('.amount').val(rs.amount);
                $('.water').val(rs.water);
                $('.meter_before').val(rs.meter_after);
                $('.meter_after').val(rs.meter_after);
            }
		});
    });

    $(".meter_after").on('keyup', function(){
       meter();
    });

    $(".meter_after").on('click', function(){
       meter();
    });

    $('.meter_after').bind('mousewheel', function(e) {
        return false;
    });

    function meter() {
        if ($('.meter_after').val().length != 0)
        {
            var meter = $('.meter_after').val() - $('.meter_before').val();
            var electricity = $('.electricity').val(meter*7);
            var amount      = parseInt($('.amount').val());
            var water       = parseInt($('.water').val());
            $('.total').val(amount+ water + (meter*7));
        }
    }

</script>
@endpush
