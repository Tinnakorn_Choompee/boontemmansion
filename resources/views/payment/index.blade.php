@extends('layouts.app')
@section('title', 'Payment')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{-- Carbon::createFromDate(null, 12, 25); --}}
                {{-- Carbon::now()->day --}}
                @role('admin')
                    @if(Carbon::createFromDate(NULL, NULL, 25)->day >= 25)
                        <a href="{{ route('payment.create') }}" class="btn btn-primary btn-sm btn-create"> <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มข้อมูลการชำระเงิน </a>
                    @endif
                @endrole
                {{ Breadcrumbs::render() }}
            </h3>
             <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> ข้อมูลการชำระเงิน <b> เดือน {{ month(Carbon::now()) }} </b>
                                @role('admin')
                                <span class="pull-right text-danger" style="font-size:20px !important"> * สามารถเพิ่มข้อมูลการชำระเงิน ได้ตั้งแต่ วันที่ 25 ถึง ทุกสิ้นเดือน เท่านั้น </span>
                                @endrole

                                @role('member')
                                <span class="pull-right text-info" style="font-size:20px !important"> สามารถดูข้อมูลการชำระเงิน ได้ตั้งแต่ วันที่ 25 ของทุกเดือนเป็นต้นไป </span>
                                @endrole
                            </h3>
                            @role('member')
                            <span class="pull-right font">  ** สามารถแจ้งชำระเงินได้ที่ ปุ่ม ดูรายละเอียด </span>
                            @endrole
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr class="font text-center">
                                            <th width="5%">#</th>
                                            <th class="text-left" width="10%">เลขห้อง</th>
                                            <th class="text-left" width="15%">ชื่อ</th>
                                            <th width="15%">วัน/เวลา การแจ้งชำระ</th>
                                            <th width="15%">สถานะ</th>
                                            <th width="15%">จำนวนเงิน</th>
                                            <th width="30%">ตัวเลือก</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($payment as $k => $rs)
                                        <tr class="text-center">
                                            <td class="text-center">{{ ++$k }}</td>
                                            <td class="text-center">{{ $rs->number }}</td>
                                            <td class="text-left">{{ $rs->user->full_name }}</td>
                                            <td>{!! dt_th($rs->created_at) !!}</td>
                                            <td class="text-center">
                                                @switch($rs->status)
                                                    @case(0)
                                                    <span class='label label-danger font'> ยังไม่ได้ชำระเงิน </span>
                                                    @break
                                                    @case(1)
                                                    <span class='label label-warning font'> รอการตรวจสอบ </span>
                                                    @break
                                                @endswitch
                                            </td>
                                            <td class="text-center">{{ number_format($rs->total) }}</td>
                                            <td class="text-center">
                                                <a class="btn btn-success" href="{{ route('payment.show', $rs->id) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                                @role('admin')
                                                @switch($rs->status)
                                                    @case(0)
                                                    <a class="btn btn-warning btn-edit" href="{{ route('payment.edit', $rs->id) }}"><i class="fa fa-pencil"></i></a>
                                                    <button class="btn btn-danger btn-del"  data-id="{{ $rs->id }}"><i class="fa fa-trash"></i></button>
                                                    {{ Form::open(['method' => 'DELETE', 'route' => ['payment.destroy', $rs->id], 'id'=>'form-delete-'.$rs->id]) }} {{ Form::close() }}
                                                    @break
                                                @endswitch
                                                @endrole
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END TABLE HOVER -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@push('scripts')
@if (session('success'))
<script>
    swal("Success!", "บันทึกข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('update'))
<script>
    swal("Updated!", "แก้ไขข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('delete'))
<script>
    swal("Deleted!", "ลบข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('process'))
<script>
    swal("Process!", "ดำเนินการรับเรื่องเรียบร้อยแล้ว", "success");
</script>
@endif
<script>
    $('.btn-del').on('click',function(){
        var id = $(this).data('id');
        swal({
            title: 'Are you sure?',
            text: "ต้องการที่จะลบ ข้อมูลการชำระเงิน นี้ใช่หรือไม่ !!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
        if (result.value) {
            $( "#form-delete-"+id ).submit();
        }
        });
    });
</script>
@endpush
