@extends('layouts.app')
@section('title', 'ข้อมูลผู้เช่าห้องพัก')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="panel panel-profile">
                <div class="clearfix">
                    <!-- LEFT COLUMN -->
                    <div class="profile">
                        <!-- PROFILE HEADER -->
                        <div class="profile-header">
                            <div class="overlay"></div>
                            <div class="profile-main">
                                <p class="font" style="font-size:50px !important"> ห้อง {{ $member->room->number }}</p>
                                <p class="font" style="font-size:25px !important"> ผู้เช่า {{ $member->full_name }}</p>
                            </div>
                        </div>
                        <!-- END PROFILE HEADER -->
                        <!-- PROFILE DETAIL -->
                        <div class="profile-detail">
                            <div class="profile-info">
                                <h5 class="heading font"> รายละเอียด </h5>
                                <ul class="list-unstyled list-justify">
                                    <li class="font"> ประเภทห้อง
                                        <span>
                                            @switch($member->room->type)
                                                @case("air")
                                                แอร์
                                                @break
                                                @case("fan")
                                                พัดลม
                                                @break
                                            @endswitch
                                        </span>
                                    </li>
                                    <li class="font"> ค่าเช่า <span> {{ $member->room->amount }} </span></li>
                                    <li class="font"> ค่าน้ำ <span> {{ $member->room->water }} </span></li>
                                    <hr>
                                    <li class="font"> อีเมล์ <span> {{ $member->email }} </span></li>
                                    <li class="font"> เบอร์โทร <span> {{ $member->phone }} </span></li>
                                    <li class="font"> ชื่อเข้าใช้ระบบ <span> {{ $member->username }} </span></li>
                                </ul>
                            </div>
                        </div>
                        <!-- END PROFILE DETAIL -->
                    </div>
                    <!-- END LEFT COLUMN -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection

