@extends('layouts.app')
@section('title', 'Login | '.env('APP_NAME'))
@section('login')
<!-- WRAPPER -->
<div id="wrapper" class="login">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box">
                <div class="left">
                    <div class="content">
                        <div class="header">
                            <div class="logo text-center">
                                {{ Html::image('image/logo-dark.png', NULL,['width'=>'80', 'height'=>'90']) }}
                            </div>
                            <p class="lead">Login to your account</p>
                        </div>
                        @if ($errors->has('username'))
                        <center>
                            <span class="invalid-feedback text-danger">
                                <span class="font" style="font-size:20px;">{!! $errors->first('username') !!}</span>
                            </span>
                        </center>
                        @endif
                        {!! Form::open(['route'=>'login', 'class'=>'login-form']) !!}
                            @csrf
                            <div class="form-group">
                                <label for="signin-username" class="control-label sr-only">Username</label>
                                <input id="username" type="username" id="signin-username" placeholder="Username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
                            </div>
                            <div class="form-group">
                                <label for="signin-password" class="control-label sr-only">Password</label>
                                <input id="password" type="password" id="signin-password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            </div>
                            {{-- <div class="form-group">
                                <label class="fancy-checkbox element-left">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old( 'remember') ? 'checked' : '' }}>
									<span>Remember me</span>
								</label>
                            </div> --}}
                            <button type="submit" class="btn btn-primary btn-lg btn-block">
                                {{ __('Login') }}
                            </button>
                            <br>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="right">
                    <div class="overlay"></div>
                    <div class="content text" style="margin-top:-40px;">
                        <h1 class="heading"> Boontem Mansion </h1>
                        <p class="font" style="font-size:25px !important"> หอพักบุญเติมแมนชั่น </p>
                        <a href=""></a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- END WRAPPER -->
@endsection
