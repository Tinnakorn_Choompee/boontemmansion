@extends('layouts.app')
@section('title', '404')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <div class="panel panel-profile">
                <div class="clearfix">
                    <!-- RIGHT COLUMN -->
                    <div class="profile">
                        <!-- PROFILE DETAIL -->
                        <div class="profile-detail">
                            <div class="profile-info text-center">
                                <h2 class="heading" style="font-size:50px !important">404</h2>
                                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
                                <p class="font" style="font-size:25px !important">
                                    คุณไม่สิทธิ์ในการใช้งานหน้านี้
                                </p>
                            </div>
                            <div class="text-center">
                                <a href="{{ URL::previous() }}" class="btn btn-warning btn-lg">  <span class="lnr lnr-arrow-left"></span> Go Back  </a>
                            </div>
                        </div>
                        <!-- END PROFILE DETAIL -->
                    </div>
                    <!-- END RIGHT COLUMN -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
