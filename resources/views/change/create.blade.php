@extends('layouts.app')
@section('title', 'เพิ่มการแจ้งเปลี่ยนแปลงห้อง')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- INPUTS -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> เพิ่มการแจ้งเปลี่ยนแปลงห้อง </h3>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="panel-body">
                            {!! Form::open(['route'=>'change.store', 'files'=> TRUE]) !!}
                                {!! Form::label('from_number', 'จากเลขห้อง', ['class'=>'font']) !!}
                                {!! Form::text('from_number', Auth::user()->room->number, ['class'=>'form-control', 'readonly']) !!}
                                <br>
                                {!! Form::label('to_number', 'เป็นเลขห้อง', ['class'=>'font']) !!}
                                {!! Form::select('to_number', $room, NULL,['class'=>'form-control', 'required']) !!}
                                <br>
                                {!! Form::label('reason', 'เหตุผลในการเปลี่ยนแปลงห้อง', ['class'=>'font']) !!}
                                {!! Form::textarea('reason', NULL, ['class'=>'form-control', 'rows'=>3, 'placeholder' =>'โปรดระบุข้อมูลเพิ่มเติม กรณีมีเหตุผลในการเปลี่ยนแปลง']) !!}
                                <br>
                                <hr>
                                <div class="text-center">
                                <h1 class="font"  style="font-size:24px !important"><b> รายละเอียดการชำระเงิน </b> </h1>
                                {{ Html::image('image/bank.png', NULL,['class'=>'img-responsive img-rounded', 'width'=>'100', 'style'=>'display: block; margin: 0 auto;margin-bottom:10px;']) }}
                                <p class="font" style="font-size:22px !important"> บัญชีธนาคารกสิกรไทย  </p>
                                <p class="font" style="font-size:22px !important"> 490-4-15428-6 </p>
                                <p class="font" style="font-size:22px !important"> นาย กฤษดา ม้ายอง </p>
                                <p class="font" style="font-size:22px !important"> สาขา มหาวิทยาลัยราชภัฏเชียงใหม่ </p>
                                <hr>
                                <p class="font text-info" style="font-size:22px !important"> ค่าใช้จ่ายในการเปลี่ยนแปลงห้องพัก 500 บาท </p>
                                <hr>
                                </div>
                                {!! Form::label('date', 'วันที่โอนเงิน', ['class'=>'font']) !!}
                                {!! Form::text('date', NULL, ['class'=>'form-control date', 'required']) !!}
                                <br>
                                {!! Form::label('time', 'เวลาที่โอนเงิน', ['class'=>'font']) !!}
                                {!! Form::text('time', NULL, ['class'=>'form-control time', 'required']) !!}
                                <br>
                                {!! Form::label('total', 'จำนวนเงิน', ['class'=>'font']) !!}
                                {!! Form::text('total', 500, ['class'=>'form-control', 'readonly']) !!}
                                <br>
                                {!! Form::label('image', 'หลักฐานการชำระเงิน', ['class'=>'font']) !!}
                                {!! Form::file('image', ['class'=>'form-control']) !!}
                                <br>
                                <button type="submit" class="btn btn-primary font"> บันทึก </button>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <!-- END INPUTS -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@push('styles')
{{ Html::style('vendor/flatpickr/flatpickr.min.css') }}
{{ Html::style('vendor/fancybox/jquery.fancybox.min.css') }}
@endpush
@push('scripts')
{{ Html::script('vendor/fancybox/jquery.fancybox.min.js') }}
{{ Html::script('vendor/flatpickr/flatpickr.js') }}
{{ Html::script('vendor/flatpickr/th.js') }}
<script>
    $(".date").flatpickr({
        locale: "th",
        minDate : "today",
        defaultDate: new Date(),
    });
    $(".time").flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
        defaultDate: new Date(),
    });
</script>
@endpush

