
@extends('layouts.app')
@section('title', 'ข้อมูลการเปลี่ยนแปลงห้อง')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="panel panel-profile">
                <div class="clearfix">
                    <!-- LEFT COLUMN -->
                    <div class="profile">
                        <!-- PROFILE HEADER -->
                        <div class="profile-header">
                            <div class="overlay"></div>
                            <div class="profile-main">
                                <p class="font" style="font-size:50px !important"> ห้อง {{ $change->from_number }} <span class="lnr lnr-arrow-right" style="font-size:25px"></span> {{ $change->to_number }}</p>
                                <p class="font" style="font-size:45px !important"> {{ $change->user->full_name }}</p>
                                @role('admin')
                                @switch($change->status)
                                @case(0)
                                <button type="button" class="btn btn-success btn-approve font btn-lg">
                                       อนุมัติการชำระเงิน
                                </button>
                                <button type="button" class="btn btn-danger btn-not font btn-lg">
                                        ไม่อนุมัติการชำระเงิน
                                </button>
                                @break
                                @endswitch
                                @endrole
                            </div>
                        </div>
                        <!-- END PROFILE HEADER -->
                        <!-- PROFILE DETAIL -->
                        <div class="profile-detail">

                            @switch($change->status)
                            @case(1)
                            @role('member')
                            <div class="text-center" style="margin-top:20px;">
                                <h6 class="heading font text-success"> อนุมัติชำระเงินเรียบร้อยแล้ว กรุณาติดต่อเจ้าหน้าที่เพื่อติดต่อวันเข้าพัก </h6>
                            </div>
                            @endrole
                            @role('admin')
                            <div class="text-center" style="margin-top:20px;">
                                <h6 class="heading font text-success"> อนุมัติชำระเงินเรียบร้อยแล้ว </h6>
                            </div>
                            @endrole
                            @break
                            @case(2)
                            <div class="text-center" style="margin-top:20px;">
                                <h6 class="heading font text-danger"> ไม่อนุมัติชำระเงิน
                                    <br><br> เนื่องจาก {{ $change->reason }}
                                </h6>
                                <br>
                                @role('member')
                                <button type="button" class="btn btn-danger font btn-lg btn-del" data-id="{{ $change->id }}">
                                        ยกเลิกการเปลี่ยนแปลงห้อง
                                </button>
                                {{ Form::open(['method' => 'DELETE', 'route' => ['change.cancel', $change->id], 'id'=>'form-delete-'.$change->id]) }} {{ Form::close() }}
                                @endrole
                            </div>
                            @break
                            @endswitch

                            @role('admin|member')
                            <div class="profile-info">
                                <h5 class="heading font"> รายละเอียดการชำระเงิน </h5>
                                <hr>
                                <ul class="list-unstyled list-justify">
                                    <li class="font"> วัน <span> {{ Carbon::parse($change->date)->format('d/m/Y') }} </span></li>
                                    <br>
                                    <li class="font"> เวลา <span> {{ Carbon::parse($change->time)->format('H:i') }} </span></li>
                                    <br>
                                    <li class="font"> รวมทั้งสิ้น <span> {{ number_format($change->total) }} </span></li>
                                    <br>
                                    <li class="font"> สถานะ
                                        @switch($change->status)
                                            @case(0)
                                            <span class='label label-warning font'> รอการตรวจสอบ </span>
                                            @break
                                            @case(1)
                                            <span class='label label-success font'> ชำระเงินเรียบร้อยแล้ว </span>
                                            @break
                                            @case(2)
                                            <span class='label label-danger font'> ไม่อนุมัติชำระเงิน </span>
                                            @break
                                        @endswitch
                                    </li>
                                    <br>
                                    @isset($change->image)
                                    <li class="font"> หลักฐานการชำระเงิน <span>
                                        <a data-fancybox="gallery" href="{{asset('image/change/'.$change->image)}}">
                                         {!! Html::image('image/change/'.$change->image, NULL, ['class'=>'img-rounded', 'width'=>'100']) !!}
                                        </a>
                                        </span></li>
                                    @endisset
                                </ul>
                            </div>
                            @endrole
                            @if($change->reason)
                            <div class="profile-info">
                                <h5 class="heading font"> เหตุผลในการเปลี่ยนแปลงห้อง </h5>
                                <hr>
                                <p>{{ $change->reason }}</p>
                            </div>
                            @endif
                        </div>
                        <!-- END PROFILE DETAIL -->
                    </div>
                    <!-- END LEFT COLUMN -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

{{ Form::open(['route'=>['change.approve', $change->id], 'id'=>'approve-form']) }}
{{ Form::hidden('approve', NULL, ['id'=>'approve']) }}
{{ Form::hidden('note', NULL, ['id'=>'note']) }}
{{ Form::close() }}

@endsection
@push('styles')
{{ Html::style('vendor/flatpickr/flatpickr.min.css') }}
{{ Html::style('vendor/fancybox/jquery.fancybox.min.css') }}
@endpush
@push('scripts')
{{ Html::script('vendor/fancybox/jquery.fancybox.min.js') }}
{{ Html::script('vendor/flatpickr/flatpickr.js') }}
{{ Html::script('vendor/flatpickr/th.js') }}
@if (session('approve'))
<script>
    swal("Success!", "อนุมัติการชำระเงินเรียบร้อยแล้ว", "success");
</script>
@elseif(session('not'))
<script>
    swal("Not allowed", "ไม่อนุมัติการชำระเงิน", "error");
</script>
@endif
<script>
    $('.btn-del').on('click',function(){
        var id = $(this).data('id');
        swal({
            title: 'Are you sure?',
            text: "ต้องการที่จะยกเลิก การเปลี่ยนแปลงห้อง นี้ใช่หรือไม่ !!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
        if (result.value) {
            $( "#form-delete-"+id ).submit();
        }
        });
    });

    $('.btn-approve').on('click',function(e){
        e.preventDefault();
        swal({
            title: 'Are you sure?',
            text: "ต้องการที่อนุมัติ การชำระเงิน นี้ใช่หรือไม่ !!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                $('#approve').val(1);
                $("#approve-form").submit();
            }
        });
    });

    $('.btn-not').on('click',function(e){
        e.preventDefault();
        swal({
        title: 'สาเหตุที่ไม่อนุมติ',
        width: 500,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
        html:
            '<input id="swal-input1" class="swal2-input">',
            preConfirm: function () {
            return new Promise(function (resolve) {
                resolve([
                    $('#swal-input1').val(),
                ])
            })
        },
        onOpen: function () {
            $('#swal-input1').focus()
        }
        }).then(function (result) {
            if (JSON.stringify(result.value).length != 4) {
                $('#approve').val(0);
                $('#note').val(result.value[0]);
                $("#approve-form").submit();
            }
        });
    });
</script>
@endpush
