@extends('layouts.app')
@section('title', 'Change')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{-- Carbon::createFromDate(null, 12, 25); --}}
                {{-- Carbon::now()->day --}}
                @role('member')
                    @if(Carbon::createFromDate(NULL, NULL, 1)->day <= 5)
                        @if(count($change) < 1)
                        <a href="{{ route('change.create') }}" class="btn btn-primary btn-sm btn-create"> <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มการแจ้งเปลี่ยนแปลงห้อง </a>
                        @endif
                    @endif
                @endrole
                {{ Breadcrumbs::render() }}
            </h3>
             <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> ข้อมูลการแจ้งเปลี่ยนแปลงห้อง
                                <span class="text-danger pull-right" style="font-size:20px"> ** การแจ้งเปลี่ยนแปลงห้อง จะมีค่าใช้จ่ายในการดำเนินการ 500 บาท </span>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr class="font text-center">
                                            <th width="5%">#</th>
                                            <th class="text-left" width="10%">จากเลขห้อง</th>
                                            <th class="text-left" width="10%">เป็นเลขห้อง</th>
                                            <th class="text-left" width="20%">ชื่อ</th>
                                            <th width="20%">วัน/เวลา แจ้งเปลี่ยนแปลง</th>
                                            <th width="10%">สถานะ</th>
                                            <th width="30%">ตัวเลือก</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($change as $k => $rs)
                                        <tr class="text-center">
                                            <td class="text-center">{{ ++$k }}</td>
                                            <td class="text-center"><b>{{ $rs->from_number }}</b></td>
                                            <td class="text-center"><b>{{ $rs->to_number }}</b></td>
                                            <td class="text-left"> {{ $rs->user->full_name }} </td>
                                            <td>{!! dt($rs->date, $rs->time) !!}</td>
                                            <td class="text-center">
                                                @switch($rs->status)
                                                    @case(0)
                                                    <span class='label label-warning font'> รอการตรวจสอบ </span>
                                                    @break
                                                    @case(1)
                                                    <span class='label label-success font'> ชำระเงินเรียบร้อยแล้ว </span>
                                                    @break
                                                    @case(2)
                                                    <span class='label label-danger font'> ไม่อนุมัติชำระเงิน </span>
                                                    @break
                                                @endswitch
                                            </td>
                                            <td class="text-center">
                                                <a class="btn btn-success" href="{{ route('change.show', $rs->id) }}"> <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                                @role('member')
                                                @switch($rs->status)
                                                    @case(0)
                                                    <a class="btn btn-warning btn-edit" href="{{ route('change.edit', $rs->id) }}"><i class="fa fa-pencil"></i></a>
                                                    @break
                                                @endswitch
                                                @endrole

                                                @role('admin')
                                                {{-- ถัดไป 1 เดือน จึง สามารถ ลบ ข้อมูล ได้ --}}
                                                @if(Carbon::parse($rs->created_at)->addMonth() < Carbon::now())
                                                @switch($rs->status)
                                                    @case(1)
                                                    <button class="btn btn-danger btn-del"  data-id="{{ $rs->id }}"><i class="fa fa-trash"></i></button>
                                                    {{ Form::open(['method' => 'DELETE', 'route' => ['change.destroy', $rs->id], 'id'=>'form-delete-'.$rs->id]) }} {{ Form::close() }}
                                                    @break
                                                @endswitch
                                                @endif
                                                @endrole

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END TABLE HOVER -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
@push('scripts')
@if (session('success'))
<script>
    swal("Success!", "บันทึกข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('update'))
<script>
    swal("Updated!", "แก้ไขข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('delete'))
<script>
    swal("Deleted!", "ลบข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('cancel'))
<script>
    swal("Cancel!", "ยกเลิกการเปลี่ยนแปลงห้องเรียบร้อยแล้ว", "success");
</script>
@endif
<script>
    $('.btn-del').on('click',function(){
        var id = $(this).data('id');
        swal({
            title: 'Are you sure?',
            text: "ต้องการที่จะลบ การเปลี่ยนแปลงห้อง นี้ใช่หรือไม่ !!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
        if (result.value) {
            $( "#form-delete-"+id ).submit();
        }
        });
    });
</script>
@endpush
