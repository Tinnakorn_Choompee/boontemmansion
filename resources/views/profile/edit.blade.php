@extends('layouts.app')
@section('title', 'Edit Profile')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- INPUTS -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title"> แก้ไขโปรไฟล์ </h3>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="panel-body">
                            {!! Form::model($profile,['route'=>'profile.update', 'method'=>'patch']) !!}
                            {!! Form::label('prefix', 'คำนำหน้า', ['class'=>'font']) !!}
                            {!! Form::select('prefix', ['นาย'=> 'นาย', 'นาง'=>'นาง'], NULL,['class'=>'form-control', 'required']) !!}
                            <br>
                            {!! Form::label('First_name', 'ชื่อ', ['class'=>'font']) !!}
                            {!! Form::text('first_name', Auth::user()->first_name, ['class'=>'form-control', 'placeholder'=>'First Name', 'required']) !!}
                            <br>
                            {!! Form::label('Last_name', 'นามสกุล', ['class'=>'font']) !!}
                            {!! Form::text('last_name', Auth::user()->last_name , ['class'=>'form-control', 'placeholder'=>'Last Name', 'required']) !!}
                            <br>
                            {!! Form::label('Email', 'อีเมล์', ['class'=>'font']) !!}
                            {!! Form::email('email', NULL, ['class'=>'form-control', 'placeholder'=>'Email', 'required']) !!}
                            <br>
                            {!! Form::label('Phone', 'เบอร์โทร', ['class'=>'font']) !!}
                            {!! Form::tel('phone', NULL, ['class'=>'form-control phone', 'placeholder'=>'Tel.', 'required' , 'maxlength'=> 10]) !!}
                            <br>
                            @role('member')
                            {!! Form::label('Card_id', 'เลขบัตรประจำตัว', ['class'=>'font']) !!}
                            {!! Form::tel('card_id', NULL, ['class'=>'form-control phone', 'placeholder'=>'Card ID.', 'required' , 'maxlength'=> 13]) !!}
                            <br>
                            {!! Form::label('Address', 'ที่อยู่', ['class'=>'font']) !!}
                            {!! Form::textarea('address', NULL, ['class'=>'form-control', 'placeholder'=>'Address', 'required', 'rows' => 3]) !!}
                             <br>
                            @endrole
                            <label for="Username" class="font"> ชื่อเข้าใช้ระบบ </label>
                            <input class="form-control" placeholder="Username" type="text" name="username" value="{{ Auth::user()->username }}" required  @role('member') readonly  @endrole>
                            <br>
                            <label for="Password" class="font"> รหัสผ่าน </label>
                            <input class="form-control" placeholder="Password" type="password" name="password">
                            <br>
                            <label for="Confirm Password" class="font">  รหัสผ่านยืนยัน </label>
                            <input class="form-control" placeholder="Password Confirm" type="password" name="password_confirmation">
                            <br>
                            <button class="btn btn-primary font" type="submit"> บันทึก </button>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <!-- END INPUTS -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection
