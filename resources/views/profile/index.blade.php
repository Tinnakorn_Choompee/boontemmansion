@extends('layouts.app')
@section('title', 'Profile')
@section('content')
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">
                {{ Breadcrumbs::render() }}
            </h3>
            <div class="panel panel-profile">
                <div class="clearfix">
                    <!-- LEFT COLUMN -->
                    <div class="profile">
                        <!-- PROFILE HEADER -->
                        <div class="profile-header">
                            <div class="overlay"></div>
                            <div class="profile-main">
                                {!! Html::image('image/user/user.png', NULL, ['class'=>'img-circle', 'width'=>'100']) !!}
                                <h3 class="name font" style="font-size:30px !important"> {{ Auth::user()->full_name }}</h3>
                            </div>
                        </div>
                        <!-- END PROFILE HEADER -->
                        <!-- PROFILE DETAIL -->
                        <div class="profile-detail">
                            <div class="profile-info">
                                <h5 class="heading font"> รายละเอียด </h5>
                                <ul class="list-unstyled list-justify">
                                    <li class="font"> ชื่อเข้าใช้งาน  <span>  {{ Auth::user()->username }}</span></li>
                                    <li class="font"> อีเมล์ <span> {{ Auth::user()->email }} </span></li>
                                    <li class="font"> เบอร์โทร <span> {{ Auth::user()->phone }} </span></li>
                                    @role('member')
                                    <li class="font"> เลขบัตรประจำตัว <span> {{ Auth::user()->card_id }} </span></li>
                                    <li class="font"> ที่อยู่ <span> {{ Auth::user()->address }} </span></li>
                                    @endrole
                                </ul>
                            </div>
                            <div class="text-center font"><a href="{{ route('profile.edit') }}" class="btn btn-primary" style="font-size:20px;"> แก้ไขโปรไฟล์ </a></div>
                        </div>
                        <!-- END PROFILE DETAIL -->
                    </div>
                    <!-- END LEFT COLUMN -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection

@push('scripts')

@if(session('update'))
 <script>
    swal({
        type: 'success',
        title: 'แก้ไขข้อมูลเรียบร้อยแล้ว',
        showConfirmButton: false,
        timer: 2000,
    })
 </script>
@endif
@endpush
