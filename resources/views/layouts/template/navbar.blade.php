<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="{{ route('home') }}">
            <span class="font"> หอพักบุญเต็มแมนชั่น </span>
        </a>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <div id="navbar-menu">
            <ul class="nav navbar-nav navbar-right">
                @role('admin')
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <span class="font"> แจ้งซ่อม </span><i class="lnr lnr-laptop-phone"></i>
                		<span class="badge bg-danger">{{ repair()['count'] != 0 ? repair()['count'] : NULL }}</span>
                	</a>
                    <ul class="dropdown-menu notifications">
                        @foreach(repair()['repair'] as $rs)
                        <li><a href="{{ route('repair.show', $rs->id) }}" class="notification-item"><span class="dot bg-primary"></span> {{ $rs->title }}</a></li>
                        @endforeach
                        <li><a href="{{ route('repair.index') }}" class="font text-center"> ดูทั้งหมด </a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <span class="font"> แจ้งเปลี่ยนแปลงห้อง </span><i class="lnr lnr-sync"></i>
                		<span class="badge bg-danger">{{ change()['count'] != 0 ? change()['count'] : NULL }}</span>
                	</a>
                    <ul class="dropdown-menu notifications">
                        @foreach(change()['change'] as $rs)
                        <li><a href="{{ route('change.show', $rs->id) }}" class="notification-item"><span class="dot bg-primary"></span> {{ $rs->from_number }} <span class="lnr lnr-arrow-right" style="font-size:16px"></span>  {{ $rs->to_number }} </a></li>
                        @endforeach
                        <li><a href="{{ route('change.index') }}" class="font text-center"> ดูทั้งหมด </a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <span class="font"> แจ้งชำระเงิน </span><i class="lnr lnr-alarm"></i>
                		<span class="badge bg-danger">{{ payment()['count'] != 0 ? payment()['count'] : NULL }}</span>
                	</a>
                    <ul class="dropdown-menu notifications">
                        @foreach(payment()['payment'] as $rs)
                        <li><a href="{{ route('payment.show', $rs->payment_id) }}" class="notification-item"><span class="dot bg-primary"></span> {{ "ห้อง ".$rs->payment->number }}</a></li>
                        @endforeach
                        <li><a href="{{ route('payment.index') }}" class="font text-center"> ดูทั้งหมด </a></li>
                    </ul>
                </li>
                @endrole

                @role('member')
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <span class="font"> แจ้งซ่อม </span><i class="lnr lnr-laptop-phone"></i>
                		<span class="badge bg-danger">{{ comfirm()['count'] != 0 ? comfirm()['count'] : NULL }}</span>
                	</a>
                    <ul class="dropdown-menu notifications">
                        @foreach(comfirm()['comfirm'] as $rs)
                        <li><a href="{{ route('repair.show', $rs->id) }}" class="notification-item"><span class="dot bg-primary"></span> {{ $rs->title }} </a></li>
                        @endforeach
                        <li><a href="{{ route('repair.index') }}" class="font text-center"> ดูทั้งหมด </a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <span class="font"> แจ้งชำระเงิน </span><i class="lnr lnr-alarm"></i>
                		<span class="badge bg-danger">{{ accept()['count'] ? accept()['count'] : NULL }}</span>
                	</a>
                    <ul class="dropdown-menu notifications">
                        @foreach(accept()['accept'] as $rs)
                        <li><a href="{{ route('payment.show', $rs->id) }}" class="notification-item"><span class="dot bg-primary"></span> {{ "ห้อง ".$rs->number }}</a></li>
                        @endforeach
                        <li><a href="{{ route('payment.index') }}" class="font text-center"> ดูทั้งหมด </a></li>
                    </ul>
                </li>
                @if(not()['count'])
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                            <span class="font"> ไม่อนุมัติชำระเงิน </span><i class="lnr lnr-cross"></i>
                		<span class="badge bg-danger">{{ not()['count'] }}</span>
                	</a>
                    <ul class="dropdown-menu notifications">
                        @foreach(not()['not'] as $rs)
                        <li><a href="{{ route('history.show', $rs->id) }}" class="notification-item"><span class="dot bg-primary"></span> {{ "ห้อง ".$rs->number }}</a></li>
                        @endforeach
                        <li><a href="{{ route('history.index') }}" class="font text-center"> ดูทั้งหมด </a></li>
                    </ul>
                </li>
                @endif
                @endrole

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            {{ Html::image('image/user/user.png', NULL, ['class'=>'img-circle']) }}
                            <span> {{ Auth::user()->full_name }}  </span> <i class="icon-submenu lnr lnr-chevron-down"></i>
                        </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('profile.index') }}"><i class="lnr lnr-user"></i> <span class="menu"> โปรไฟล์ </span></a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form-menu').submit();">
                                <i class="lnr lnr-exit"></i> <span class="menu"> ออกจากระบบ </span>
                            </a>
                            {!! Form::open(['url' => 'logout', 'id'=>'logout-form-menu']) !!} {!! Form::close() !!}
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</nav>
<!-- END NAVBAR -->
