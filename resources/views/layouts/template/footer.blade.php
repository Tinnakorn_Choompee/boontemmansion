<div class="clearfix"></div>
<footer>
    <div class="container-fluid">
        <p class="copyright">&copy; {{ Carbon::now()->year }} <a href="{{ route('home') }}"> All Rights Reserved </p>
     </div>
</footer>
