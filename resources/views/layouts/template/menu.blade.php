<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li>
                    <a href="{{ route('home') }}" class="{{ starts_with(Request::route()->getName(), 'home') ? 'active' : '' }}">
                        <i class="lnr lnr-home"></i>
                        <span class="menu"> หน้าหลัก </span>
                    </a>
                </li>
                @foreach(menu() as $k => $v)
                @role($v['role'])
                <li>
                    <a href="{{ $v['route'] }}" class="{{ starts_with(Request::route()->getName(), $k) ? 'active' : '' }}" id="{{ $k }}">
                        <i class="lnr {{ $v['icon'] }}"></i>
                        <span class="menu"> {{ $v['name'] }} </span>
                    </a>
                </li>
                @endrole
                @endforeach
                @role('owner')
                <li>
                    <a href="#report" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span class="menu">รายงาน</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="report" class="collapse">
                        <ul class="nav" >
                            <li><a href="{{ route('room.index')    }}" class="menu">ห้องพัก</a></li>
                            <li><a href="{{ route('member.index')  }}" class="menu">ผู้เช่าหอพัก</a></li>
                            <li><a href="{{ route('change.index')  }}" class="menu">แจ้งเปลี่ยนแปลงห้อง</a></li>
                            <li><a href="{{ route('payment.index') }}" class="menu">การชำระเงิน</a></li>
                            <li><a href="{{ route('history.index') }}" class="menu">ประวัติการชำระเงิน</a></li>
                        </ul>
                    </div>
                </li>
                @endrole
                <li>
                    <a href="{{ route('profile.index') }}" class="{{ starts_with(Request::route()->getName(), "profile") ? 'active' : '' }}">
                        <i class="lnr lnr-user"></i> <span class="menu"> โปรไฟล์ </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="lnr lnr-exit"></i> <span class="menu"> ออกจากระบบ </span>
                    </a> {!! Form::open(['url' => 'logout', 'id'=>'logout-form']) !!} {!! Form::close() !!}
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- END LEFT SIDEBAR -->
