<!DOCTYPE html>
<html lang="en" class="fullscreen-bg">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> @yield('title') </title>
    <!-- Favicon -->
    <link rel="icon" href="/image/favicon.png" type="image/x-icon" />
    <!-- VENDOR CSS -->
    {{ Html::style('css/bootstrap.min.css') }}
    {{ Html::style('vendor/font-awesome/css/font-awesome.min.css') }}
    {{ Html::style('vendor/linearicons/style.css') }}
    {{ Html::style('vendor/chartist/css/chartist-custom.css') }}
    {{ Html::style('vendor/iCheck/all.css') }}
	<!-- MAIN CSS -->
    {{ Html::style('css/main.css') }}
    <!-- Font -->
    {{ Html::style('css/style.css') }}
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    {{ Html::style('css/demo.css') }}
    {{ Html::style('css/animate.css') }}
    {{ Html::style('css/hover-min.css') }}
    <!-- DataTables -->
    {{ Html::style('vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
    {{ Html::style('vendor/sweetalert2/sweetalert2.min.css') }}
    @stack('styles')
</head>
<body>
    @guest
        @yield('login')
    @else
        <!-- WRAPPER -->
        <div id="wrapper">
            @include('layouts.template.navbar')
            @include('layouts.template.menu')
            @yield('content')
            @include('layouts.template.footer')
        </div>
        <!-- END WRAPPER -->
    @endguest
</body>
    <!-- Javascript -->
    {{ Html::script('js/jquery-3.3.1.min.js') }}
    {{ Html::script('vendor/bootstrap/js/bootstrap.min.js') }}
    {{ Html::script('vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}
    {{ Html::script('vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}
    {{ Html::script('vendor/chartist/js/chartist.min.js') }}
    {{ Html::script('vendor/sweetalert2/sweetalert2.min.js') }}
	{{ Html::script('vendor/iCheck/icheck.min.js') }}
    {{ Html::script('js/klorofil-common.js') }}
    <!-- DataTables -->
    {{ Html::script('vendor/datatables.net/js/jquery.dataTables.js') }}
    {{ Html::script('vendor/datatables.net-bs/js/dataTables.bootstrap.min.js')}}
    @stack('scripts')
</html>
