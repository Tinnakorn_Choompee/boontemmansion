<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('changes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->Integer('from_number');
            $table->Integer('to_number');
            $table->text('reason')->nullable();

            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('read')->default(0);
            $table->text('note')->nullable();// Admin

            $table->float('total', 8, 2)->default(0);

            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('changes');
    }
}
