<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->Integer('number');
            $table->string('type')->nullable();
            $table->float('amount', 8, 2)->default(0);
            $table->float('water', 8, 2)->default(0);
            $table->float('meter_before', 8, 2)->default(0);
            $table->float('meter_after', 8, 2)->default(0);
            $table->float('unit', 8, 2)->default(0);
            $table->float('electricity', 8, 2)->nullable();
            $table->float('total', 8, 2)->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
