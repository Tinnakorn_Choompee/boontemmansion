<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->unsignedInteger('payment_id');
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->float('total', 8, 2)->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('read')->default(0);   // Admin
            $table->tinyinteger('approve')->default(0);
            $table->text('note')->nullable();
            $table->timestamps();
            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
