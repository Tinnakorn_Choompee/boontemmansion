<?php

use Illuminate\Database\Seeder;
use App\Models\Room;

class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=101; $i <= 110; $i++):
            $room = new Room;
            $room->number = $i;
            $room->type   = "air";
            $room->amount = 3500;
            $room->water  = 120;
            $room->meter_before = 0;
            $room->meter_after  = 150;
            $room->save();
        endfor;

        for($i=201; $i <= 210; $i++):
            $room = new Room;
            $room->number = $i;
            $room->type   = "fan";
            $room->amount = 3000;
            $room->water  = 120;
            $room->save();
        endfor;

        for($i=301; $i <= 310; $i++):
            $room = new Room;
            $room->number = $i;
            $room->type   = "fan";
            $room->amount = 3000;
            $room->water  = 120;
            $room->save();
        endfor;

        for($i=401; $i <= 410; $i++):
            $room = new Room;
            $room->number = $i;
            $room->type   = "fan";
            $room->amount = 3000;
            $room->water  = 120;
            $room->save();
        endfor;
    }
}
