<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->prefix     = "Owner";
        $user->first_name = "Boontem";
        $user->last_name  = "Mansion";
        $user->email      = "owner@gmail.com";
        $user->phone      = "0000000000";
        $user->username   = "owner";
        $user->password   = bcrypt('123456');
        $user->save();
        $user->assignRole('owner');

        $user = new User;
        $user->prefix     = "นาย";
        $user->first_name = "ทินกร";
        $user->last_name  = "จุมปี";
        $user->email      = "admin@gmail.com";
        $user->phone      = "0918595385";
        $user->username   = "admin";
        $user->password   = bcrypt('123456');
        $user->save();
        $user->assignRole('admin');
    }
}
