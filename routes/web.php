<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function() { return redirect()->route('login'); })->name('/');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function () {
    // Profile
    Route::get('profile', 'ProfileController@index')->name('profile.index');
    Route::get('profile/edit', 'ProfileController@edit')->name('profile.edit');
    Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
    // User
    Route::resource('user',   'UserController');
    // Room
    Route::resource('room', 'RoomController');
    // Member
    Route::resource('member', 'MemberController');
    // Repair
    Route::resource('repair', 'RepairController');
    Route::patch('/process/repair', 'RepairController@process')->name('process.repair');
    Route::patch('/success/repair', 'RepairController@success')->name('success.repair');
    // Change
    Route::resource('change', 'ChangeController');
    Route::post('approve/change/{change}', 'ChangeController@approve')->name('change.approve');
    Route::delete('cancel/change/{change}', 'ChangeController@cancel')->name('change.cancel');
    // Payment
    Route::resource('payment', 'PaymentController');
    Route::post('select/payment', 'PaymentController@select')->name('select.payment');
    Route::post('notification/payment', 'PaymentController@notification')->name('notification.payment');
    Route::post('notification/edit', 'PaymentController@notification_edit')->name('notification.edit');
    Route::post('approve/payment/{payment}', 'PaymentController@approve')->name('payment.approve');
    // History
    Route::get('history', 'HistoryController@index')->name('history.index');
    Route::get('history/{history}', 'HistoryController@show')->name('history.show');
    Route::get('history/print/{history}', 'HistoryController@print')->name('history.print');
});

