<?php
// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('หน้าหลัก', route('home'));
});
// Profile
Breadcrumbs::for('profile.index', function ($trail) {
    $trail->parent('home');
    $trail->push('โปรไฟล์', route('profile.index'));
});
Breadcrumbs::for('profile.edit', function ($trail) {
    $trail->parent('profile.index');
    $trail->push('แก้ไขโปรไฟล์', route('profile.edit'));
});
// User
Breadcrumbs::for('user.index', function ($trail) {
    $trail->parent('home');
    $trail->push('ผู้ใช้งาน', route('user.index'));
});
Breadcrumbs::for('user.create', function ($trail) {
    $trail->parent('user.index');
    $trail->push('เพิ่มผู้ใช้งาน', route('user.create'));
});
Breadcrumbs::for('user.edit', function ($trail, $id) {
    $trail->parent('user.index');
    $trail->push('แก้ไขผู้ใช้งาน', route('user.edit', $id));
});

// Room
Breadcrumbs::for('room.index', function ($trail) {
    $trail->parent('home');
    $trail->push('ห้องพัก', route('room.index'));
});
Breadcrumbs::for('room.create', function ($trail) {
    $trail->parent('room.index');
    $trail->push('เพิ่มห้องพัก', route('room.create'));
});
Breadcrumbs::for('room.show', function ($trail, $id) {
    $trail->parent('room.index');
    $trail->push('ข้อมูลห้องพัก', route('room.show', $id));
});
Breadcrumbs::for('room.edit', function ($trail, $id) {
    $trail->parent('room.index');
    $trail->push('แก้ไขห้องพัก', route('room.edit', $id));
});

// Member
Breadcrumbs::for('member.index', function ($trail) {
    $trail->parent('home');
    $trail->push('ผู้เช่าหอพัก', route('member.index'));
});
Breadcrumbs::for('member.create', function ($trail) {
    $trail->parent('member.index');
    $trail->push('เพิ่มผู้เช่าหอพัก', route('member.create'));
});
Breadcrumbs::for('member.show', function ($trail, $id) {
    $trail->parent('member.index');
    $trail->push('ข้อมูลผู้เช่าหอพัก', route('member.show', $id));
});
Breadcrumbs::for('member.edit', function ($trail, $id) {
    $trail->parent('member.index');
    $trail->push('แก้ไขผู้เช่าหอพัก', route('member.edit', $id));
});

// Repair
Breadcrumbs::for('repair.index', function ($trail) {
    $trail->parent('home');
    $trail->push('แจ้งซ่อม', route('repair.index'));
});
Breadcrumbs::for('repair.create', function ($trail) {
    $trail->parent('repair.index');
    $trail->push('เพิ่มข้อมูลแจ้งซ่อม', route('repair.create'));
});
Breadcrumbs::for('repair.show', function ($trail, $id) {
    $trail->parent('repair.index');
    $trail->push('ข้อมูลแจ้งซ่อม', route('repair.show', $id));
});
Breadcrumbs::for('repair.edit', function ($trail, $id) {
    $trail->parent('repair.index');
    $trail->push('แก้ไขข้อมูลแจ้งซ่อม', route('repair.edit', $id));
});

// Payment
Breadcrumbs::for('payment.index', function ($trail) {
    $trail->parent('home');
    $trail->push('การชำระเงิน', route('payment.index'));
});
Breadcrumbs::for('payment.create', function ($trail) {
    $trail->parent('payment.index');
    $trail->push('เพิ่มการชำระเงิน', route('payment.create'));
});
Breadcrumbs::for('payment.show', function ($trail, $id) {
    $trail->parent('payment.index');
    $trail->push('ข้อมูลการชำระเงิน', route('payment.show', $id));
});
Breadcrumbs::for('payment.edit', function ($trail, $id) {
    $trail->parent('payment.index');
    $trail->push('แก้ไขการชำระเงิน', route('payment.edit', $id));
});

// History
Breadcrumbs::for('history.index', function ($trail) {
    $trail->parent('home');
    $trail->push('ประวัติการชำระเงิน', route('history.index'));
});
Breadcrumbs::for('history.show', function ($trail, $id) {
    $trail->parent('history.index');
    $trail->push('ข้อมูลการชำระเงิน', route('history.show', $id));
});

// Change
Breadcrumbs::for('change.index', function ($trail) {
    $trail->parent('home');
    $trail->push('แจ้งเปลี่ยนแปลงห้อง', route('change.index'));
});
Breadcrumbs::for('change.create', function ($trail) {
    $trail->parent('change.index');
    $trail->push('เพิ่มการแจ้งเปลี่ยนแปลงห้อง', route('change.create'));
});
Breadcrumbs::for('change.show', function ($trail, $id) {
    $trail->parent('change.index');
    $trail->push('ข้อมูลการเปลี่ยนแปลงห้อง', route('change.show', $id));
});
Breadcrumbs::for('change.edit', function ($trail, $id) {
    $trail->parent('change.index');
    $trail->push('แก้ไขข้อมูลการเปลี่ยนแปลงห้อง', route('change.edit', $id));
});
?>
