<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Detail;
use App\Models\Room;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use File;
use Carbon;
use Intervention\Image\ImageManagerStatic as Image;

class PaymentController extends Controller
{
    function __construct()
    {
         $this->middleware('role:owner|admin|member');
    }

    public function index()
    {
        $payment = Auth::user()->hasRole('admin') || Auth::user()->hasRole('owner') ?
        Payment::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->
        whereIn('status', [0,1])->orderBy('created_at', 'DESC')->get():
        Payment::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->
        where('user_id', Auth::user()->id)->whereIn('status', [0,1])->orderBy('created_at', 'DESC')->get();
        return view('payment.index')->withPayment($payment);
    }

    public function create()
    {
        $all_room = Room::all();
        foreach($all_room as $rs):
            if(Carbon::parse($rs->payment_date) < Carbon::now()):
                $rs->payment_date = NULL;
                $rs->status       = 0;
                $rs->save();
            endif;
        endforeach;
        // เมื่อขึ้น เดือนใหม่ ระบบ จะ จัดการ status ห้องให้เป็น 0 ใหม่
        // if(Carbon::parse($rs->payment_date) <  Carbon::create(NULL,12,25) ):
        $room = Room::where('status', 0)->whereNotNull('user_id')->pluck('number', 'number');
        return view('payment.create')->withRoom($room);
    }

    public function store(Request $request)
    {
        $room = Room::where('number', $request->number)->first();
        $room->status       = 1;
        $room->payment_date = Carbon::create(NULL,NULL, Carbon::now()->daysInMonth);
        $room->save();

        $payment = new Payment;
        $payment->user_id      = $room->user_id;
        $payment->number       = $request->number;
        $payment->type         = $request->type;
        $payment->amount       = $request->amount;
        $payment->water        = $request->water;
        $payment->meter_before = $request->meter_before;
        $payment->meter_after  = $request->meter_after;
        $payment->unit         = $request->meter_after - $request->meter_before;
        $payment->electricity  = $request->electricity;
        $payment->total        = $request->total;
        $payment->save();
        return redirect()->route('payment.index')->with("success", 'Success');
    }

    public function show(Payment $payment)
    {
        return view('payment.show')->withPayment($payment);
    }

    public function edit(Payment $payment)
    {
        return view('payment.edit')->withPayment($payment);
    }

    public function update(Request $request, Payment $payment)
    {
        $payment->meter_after  = $request->meter_after;
        $payment->unit         = $request->meter_after - $request->meter_before;
        $payment->electricity  = $request->electricity;
        $payment->total        = $request->total;
        $payment->save();
        return redirect()->route('payment.index')->with("update", 'Success');
    }

    public function destroy(Payment $payment)
    {
        $user = User::find($payment->user_id);
        $room = Room::find($user->room->id);
        $room->status = 0;
        $room->save();

        Payment::destroy($payment->id);
        return redirect()->route('payment.index')->with('delete', 'Success');
    }

    public function select(Request $request)
    {
        $room = Room::where('number', $request->room)->first();
        return response()->json($room);
    }

    public function notification(Request $request)
    {
        $message = ['date.required' => 'กรุณาระบุวันที่โอน', 'time.required' => 'กรุณาระบุเวลาที่โอน', 'total' => 'กรุณาระบุจำนวนเงินที่โอน'];
        $this->validate($request, ['date' => 'required', 'time' => 'required' , 'total' => 'required'], $message);

        $payment = Payment::find($request->payment_id);
        $payment->status = 1;
        $payment->save();

        $detail = New Detail;
        $detail->payment_id = $request->payment_id;
        $detail->date       = $request->date;
        $detail->time       = $request->time;
        $detail->total      = $request->total;

        if($request->hasFile('image')) {
            $file = $request->image;
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());
            $image_width  = Image::make($file->getRealPath())->width();
            $image_heigth = Image::make($file->getRealPath())->height();
            $image_resize->resize($image_width, $image_heigth);
            $image_resize->save('image/payment/'.$name);
            $detail->image = $name;
        }
        $detail->save();

        return redirect()->back()->with("payment", 'Success');
    }

    public function notification_edit(Request $request)
    {
        $message = ['date.required' => 'กรุณาระบุวันที่โอน', 'time.required' => 'กรุณาระบุเวลาที่โอน', 'total' => 'กรุณาระบุจำนวนเงินที่โอน'];
        $this->validate($request, ['date' => 'required', 'time' => 'required' , 'total' => 'required'], $message);

        $payment = Payment::find($request->payment_id);
        $payment->status = 1;
        $payment->save();

        $payment->detail->date    = $request->date;
        $payment->detail->time    = $request->time;
        $payment->detail->total   = $request->total;
        $payment->detail->read    = 0;

        if($payment->detail->image) {
            File::delete('image/payment/'.$payment->detail->image);
        }

        if($request->hasFile('image')) {
            $file = $request->image;
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());
            $image_width  = Image::make($file->getRealPath())->width();
            $image_heigth = Image::make($file->getRealPath())->height();
            $image_resize->resize($image_width, $image_heigth);
            $image_resize->save('image/payment/'.$name);
            $payment->detail->image = $name;
        }
        $payment->detail->save();

        return redirect()->route('payment.show', $payment->id)->with("payment", 'Success');
    }

    public function approve(Request $request,Payment $payment)
    {
        $payment->detail->read = 1;
        switch ($request->approve) :
            case 0:
                $payment->status = 3;
                $payment->save();

                $payment->detail->approve = 2; // ไม่อนุมัติ
                $payment->detail->note    = $request->note;
                $payment->detail->save();
                return redirect()->route('history.index')->with('not', 'Not Successfully!');
            break;
            case 1:
                $payment->status = 2;
                $payment->save();

                $payment->detail->approve = 1; // อนุมัติ
                $payment->detail->save();
                return redirect()->route('history.index')->with('approve', 'Approve Successfully!');
            break;
        endswitch;
    }
}
