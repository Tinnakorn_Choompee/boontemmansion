<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    function __construct()
    {
        $this->middleware('role:admin|owner');
    }

    public function index()
    {
        return view('room.index')->withRooms(Room::all());
    }

    public function create()
    {
        return view('room.create');
    }

    public function store(Request $request)
    {
        $message = [
            'number.unique' => 'มีห้องพักนี้แล้ว'
        ];
        $request->validate([
            'number' => 'required|unique:rooms',
        ],$message);
        Room::create($request->all());
        return redirect()->route('room.index')->with('success', "Success");
    }

    public function show(Room $room)
    {
        return view('room.show')->withRoom($room);
    }

    public function edit(Room $room)
    {
        return view('room.edit')->withRoom($room);
    }

    public function update(Request $request, Room $room)
    {
        $message = [
            'number.unique' => 'มีห้องพักนี้แล้ว'
        ];
        $request->validate([
            'number' => 'required|unique:rooms,number,'.$room->id,
        ],$message);
        Room::updateOrCreate(['id'=>$room->id],$request->all());
        return redirect()->route('room.index')->with('success', "Success");
    }

    public function destroy(Room $room)
    {
        Room::destroy($room->id);
        return redirect()->route('room.index')->with('delete', "Success");
    }
}
