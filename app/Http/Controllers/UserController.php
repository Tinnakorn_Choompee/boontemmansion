<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    function __construct()
    {
         $this->middleware('role:owner');
    }

    public function index()
    {
        return view('user.index')->withUsers(User::role('admin')->get());
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $message = [
            'username.unique' => 'มีผู้ใช้นี้แล้ว',
            'phone.unique'    => 'มีเบอร์โทรแล้ว',
        ];
        $request->validate([
            'username' => 'required|unique:users',
            'phone'    => 'required|unique:users',
            'password' => 'required',
            'email'    => 'required|email|unique:users',
        ]);
        $user = new User;
        $user->prefix     = $request->prefix;
        $user->first_name = $request->first_name;
        $user->last_name  = $request->last_name;
        $user->email      = $request->email;
        $user->phone      = $request->phone;
        $user->username   = $request->username;
        $user->password   = bcrypt('password');
        $user->save();
        $user->assignRole('admin');
        return redirect()->route('user.index')->with('success', "Success");
    }

    public function edit(User $user)
    {
        return view('user.edit')->withUser($user);
    }

    public function update(Request $request, User $user)
    {
        $message = [
            'username.unique' => 'มีผู้ใช้นี้แล้ว',
            'phone.unique'    => 'มีเบอร์โทรแล้ว',
        ];
        $request->validate([
            'username' => 'required|unique:users,username,'.$user->id,
            'phone'    => 'required|unique:users,phone,'.$user->id,
            'email'    => 'required|email|unique:users,email,'.$user->id
        ], $message);
        $user->prefix     = $request->prefix;
        $user->first_name = $request->first_name;
        $user->last_name  = $request->last_name;
        $user->email      = $request->email;
        $user->phone      = $request->phone;
        $user->username   = $request->username;
        $user->password   = isset($request->password) ? bcrypt($request->password) : $user->password;
        $user->save();
        return redirect()->route('user.index')->with('update', "Success");
    }

    public function destroy(User $user)
    {
        User::destroy($user->id);
        return redirect()->route('user.index')->with('delete', "Success");
    }
}
