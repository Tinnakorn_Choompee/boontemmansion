<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Room;
use App\Models\Payment;
use App\Models\Repair;
use App\Models\Change;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        for ($i=1; $i <= 12; $i++) :
            $payment[] = Payment::where('status', 2)->whereMonth('created_at', $i)->whereYear('created_at', date('Y'))->sum('total');
        endfor;

        for ($i=1; $i <= 12; $i++) :
            $unit[]    = Payment::where('status', 2)->whereMonth('created_at', $i)->whereYear('created_at', date('Y'))->sum('unit');
        endfor;

        for ($i=1; $i <= 12; $i++) :
            $repair[]  = Repair::where('status', 2)->whereMonth('created_at', $i)->whereYear('created_at', date('Y'))->count();
        endfor;

        for ($i=1; $i <= 12; $i++) :
            $change[]  = Change::where('status', 1)->whereMonth('created_at', $i)->whereYear('created_at', date('Y'))->count();
        endfor;

        $member = User::role('member')->count();
        $room   = Room::whereNull('user_id')->count();
        return view('home')
        ->withMember($member)
        ->withRoom($room)
        ->withPayment($payment)
        ->withUnit($unit)
        ->withRepair($repair)
        ->withChange($change)
        ;
    }
}
