<?php

namespace App\Http\Controllers;

use App\Models\Change;
use App\Models\Room;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class ChangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $change = Auth::user()->hasRole('admin') ? Change::orderBy('created_at', 'DESC')->get() : Change::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        return view('change.index')->withChange($change);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $room = Room::whereNull('user_id')->pluck('number', 'number');
        return view('change.create')->withRoom($room);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = ['date.required' => 'กรุณาระบุวันที่โอน', 'time.required' => 'กรุณาระบุเวลาที่โอน', 'total' => 'กรุณาระบุจำนวนเงินที่โอน'];
        $this->validate($request, ['date' => 'required', 'time' => 'required' , 'total' => 'required'], $message);

        $change              = New Change;
        $change->user_id     = Auth::user()->id;
        $change->from_number = $request->from_number;
        $change->to_number   = $request->to_number;
        $change->reason      = $request->reason ? $request->reason : NULL;
        $change->date        = $request->date;
        $change->time        = $request->time;
        $change->total       = $request->total;

        if($request->hasFile('image')) {
            $file = $request->image;
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());
            $image_width  = Image::make($file->getRealPath())->width();
            $image_heigth = Image::make($file->getRealPath())->height();
            $image_resize->resize($image_width, $image_heigth);
            $image_resize->save('image/change/'.$name);
            $change->image = $name;
        }
        $change->save();

        return redirect()->route('change.index')->with("success", 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Change  $change
     * @return \Illuminate\Http\Response
     */
    public function show(Change $change)
    {
        return view('change.show')->withChange($change);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Change  $change
     * @return \Illuminate\Http\Response
     */
    public function edit(Change $change)
    {
        $room = Room::whereNull('user_id')->pluck('number', 'number');
        return view('change.edit')->withChange($change)->withRoom($room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Change  $change
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Change $change)
    {
        $change->from_number = $request->from_number;
        $change->to_number   = $request->to_number;
        $change->save();
        return redirect()->route('change.index')->with("update", 'Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Change  $change
     * @return \Illuminate\Http\Response
     */
    public function destroy(Change $change)
    {
        $change->image ? File::delete('image/change/'.$change->image) : NULL;
        $change->delete();
        return redirect()->route('change.index')->with('delete', 'Success');
    }

    public function cancel(Change $change)
    {
        $change->image ? File::delete('image/change/'.$change->image) : NULL;
        $change->delete();
        return redirect()->route('change.index')->with('cancel', 'Success');
    }

    public function approve(Request $request,Change $change)
    {
        $change->read = 1;
        switch ($request->approve) :
            case 0: // ไม่อนุมัติ
                $change->status = 2;
                $change->note   = $request->note;
                $change->save();
                return redirect()->back()->with('not', 'Not Successfully!');
            break;
            case 1: // อนุมัติ
                $user = User::find($change->user_id);
                $user->username = "boontem".$change->to_number;
                $user->save();

                $from = Room::where('number', $change->from_number)->first();
                $from->user_id = NULL;
                $from->save();
                $to = Room::where('number', $change->to_number)->first();
                $to->user_id = $change->user_id;
                $to->save();

                $change->status = 1;
                $change->save();
                return redirect()->back()->with('approve', 'Approve Successfully!');
            break;
        endswitch;
    }
}
