<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Room;

class MemberController extends Controller
{
    function __construct()
    {
         $this->middleware('role:admin|owner');
    }

    public function index()
    {
        $member = User::role('member')->get();
        return view('member.index')->withMembers($member);
    }

    public function create()
    {
        $room = Room::whereNull('user_id')->pluck('number', 'number');
        return view('member.create')->withRoom($room);
    }

    public function store(Request $request)
    {
        $message = [
            'username.unique' => 'มีผู้ใช้นี้แล้ว',
            'phone.unique'    => 'มีเบอร์โทรแล้ว',
            'card_id.unique'  => 'มีเลขบัตรนี้แล้ว',
            'email.unique'    => 'มีอีเมล์นี้แล้ว',
        ];
        $request->validate([
            'username' => 'required|unique:users',
            'phone'    => 'required|unique:users',
            'card_id'  => 'required|unique:users',
            'email'    => 'required|email|unique:users',
        ], $message);
        $user = new User;
        $user->prefix     = $request->prefix;
        $user->first_name = $request->first_name;
        $user->last_name  = $request->last_name;
        $user->email      = $request->email;
        $user->phone      = $request->phone;
        $user->card_id    = $request->card_id;
        $user->address    = $request->address;
        $user->username   = "boontem".$request->username;
        $user->password   = bcrypt('password');
        $user->save();
        $user->assignRole('member');

        $room = Room::where('number', $request->username)->first();
        $room->user_id = $user->id;
        $room->save();

        return redirect()->route('member.index')->with('success', "Success");
    }

    public function show($id)
    {
        $member = User::find($id);
        return view('member.show')->withMember($member);
    }

    public function edit($id)
    {
        $member = User::find($id);
        $room   = Room::whereNull('user_id')->pluck('number', 'number');
        return view('member.edit')->withMember($member)->withRoom($room);
    }

    public function update(Request $request, $id)
    {
        $message = [
            'username.unique' => 'มีผู้ใช้นี้แล้ว',
            'phone.unique'    => 'มีเบอร์โทรแล้ว',
            'card_id.unique'  => 'มีเลขบัตรนี้แล้ว',
            'email.unique'    => 'มีอีเมล์นี้แล้ว',
        ];
        $request->validate([
            'username' => 'required|unique:users,username,'.$id,
            'phone'    => 'required|unique:users,phone,'.$id,
            'card_id'  => 'required|unique:users,card_id,'.$id,
            'email'    => 'required|email|unique:users,email,'.$id,
        ], $message);

        $user = User::find($id);
        $user->prefix     = $request->prefix;
        $user->first_name = $request->first_name;
        $user->last_name  = $request->last_name;
        $user->email      = $request->email;
        $user->phone      = $request->phone;
        $user->card_id    = $request->card_id;
        $user->address    = $request->address;
        $user->username   = "boontem".$request->username;
        $user->password   = isset($request->password) ? bcrypt($request->password) : $user->password;
        $user->save();

        $room = Room::where('user_id', $user->id)->first();
        $room->user_id = NULL;
        $room->save();

        $room = Room::where('number', $request->username)->first();
        $room->user_id = $user->id;
        $room->save();

        return redirect()->route('member.index')->with('update', "Success");
    }

    public function destroy($id)
    {
        Room::where('user_id', $id)->update(['user_id'=> NULL, 'status' => 0]);
        User::destroy($id);
        return redirect()->route('member.index')->with('delete', 'Success');
    }
}
