<?php

namespace App\Http\Controllers;

use App\Models\Repair;
use Illuminate\Http\Request;
use Auth;

class RepairController extends Controller
{
    function __construct()
    {
         $this->middleware('role:owner|admin|member');
    }

    public function index()
    {
        $repair = Auth::user()->hasRole('admin') || Auth::user()->hasRole('owner') ? Repair::orderBy('created_at', 'DESC')->get() : Repair::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        return view('repair.index')->withRepair($repair);
    }

    public function create()
    {
        return view('repair.create');
    }

    public function store(Request $request)
    {
        $repair = new Repair;
        $repair->user_id = Auth::user()->id;
        $repair->number  = Auth::user()->room->number;
        $repair->title   = $request->title;
        $repair->detail  = $request->detail;
        $repair->save();
        return redirect()->route('repair.index')->with('success', 'Success');
    }

    public function show(Repair $repair)
    {
        $repair->read = 1;
        $repair->save();
        return view('repair.show')->withRepair($repair);
    }


    public function edit(Repair $repair)
    {
        return view('repair.edit')->withRepair($repair);
    }

    public function update(Request $request, Repair $repair)
    {
        $repair->user_id = Auth::user()->id;
        $repair->number  = Auth::user()->room->number;
        $repair->title   = $request->title;
        $repair->detail  = $request->detail;
        $repair->save();
        return redirect()->route('repair.index')->with('update', 'Success');
    }

    public function destroy(Repair $repair)
    {
        Repair::destroy($repair->id);
        return redirect()->route('repair.index')->with('delete', 'Success');
    }

    public function process(Request $request)
    {
        $repair = Repair::find($request->id);
        $repair->status = 1;
        $repair->datetime_repair = $request->date." ".$request->time;
        $repair->save();
        return redirect()->route('repair.index')->with('process', 'Success');
    }

    public function success(Request $request)
    {
        $repair = Repair::find($request->id);
        $repair->status = 2;
        $repair->save();
        return response()->json("success");
    }
}
