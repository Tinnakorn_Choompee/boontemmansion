<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use File;

class ProfileController extends Controller
{
    public function index()
    {
        return view('profile.index');
    }

    public function edit()
    {
        return view('profile.edit')->withProfile(User::find(Auth::user()->id));
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $request->validate([
            'username' => 'required|unique:users,username,'.$user->id,
            'email'    => 'required|email|unique:users,email,'.$user->id,
            'password' => 'confirmed',
        ]);
        $user->prefix     = $request->prefix;
        $user->first_name = $request->first_name;
        $user->last_name  = $request->last_name;
        $user->email      = $request->email;
        $user->phone      = $request->phone;

        $user->card_id    = isset($request->card_id) ? $request->card_id : NULL;
        $user->address    = isset($request->address) ? $request->address : NULL;

        $user->password   = isset($request->password) ? bcrypt($request->password) : $user->password;
        $user->save();
        return redirect()->route('profile.index')->with('update', "Success");
    }
}
