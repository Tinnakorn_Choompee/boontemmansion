<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Auth;
use PDF;

class HistoryController extends Controller
{

    public function index()
    {
       $payment = Auth::user()->hasRole('admin') || Auth::user()->hasRole('owner') ?
       Payment::whereIn('status', [2,3])->orderBy('created_at', 'DESC')->get() :
       Payment::where('user_id', Auth::user()->id)->whereIn('status', [2,3])->orderBy('created_at', 'DESC')->get();
       $count = Payment::where('user_id', Auth::user()->id)->where('status', 3)->count();
       return view('history.index')->withPayment($payment)->withCount($count);
    }

    public function show(Payment $history)
    {
        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('owner') ) {
            $payment = $history;
        } else {
            if($history->user_id == Auth::user()->id) {
                $payment = $history;
            } else {
                return view('404');
            }
        }
        return view('history.show')->withPayment($payment);
    }

    public function print(Payment $history)
    {
        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('owner') ) {
            return PDF::loadView('history.print', compact('history'))->stream('payment.pdf');
        } else {
            if($history->user_id == Auth::user()->id) {
                $pdf = PDF::loadView('history.print', compact('history'));
                return $pdf->stream('payment.pdf');
            } else {
                return view('404');
            }
        }

    }
}
