<?php
use App\Models\Repair;
use App\Models\Detail;
use App\Models\Payment;
use App\Models\Change;

function menu()
{
    return [
        'user'    => ['role' => 'owner',        'route'=> route('user.index')     , 'icon' => 'lnr-users'       ,'name' => 'ผู้ใช้งาน'],
        'room'    => ['role' => 'admin',        'route'=> route('room.index')     , 'icon' => 'lnr-apartment'   ,'name' => 'ห้องพัก'],
        'member'  => ['role' => 'admin',        'route'=> route('member.index')   , 'icon' => 'lnr-users'       ,'name' => 'ผู้เช่าหอพัก'],
        'repair'  => ['role' => 'admin|member', 'route'=> route('repair.index')   , 'icon' => 'lnr-laptop-phone','name' => 'แจ้งซ่อมแซมอุปกรณ์'],
        'change'  => ['role' => 'admin|member', 'route'=> route('change.index')   , 'icon' => 'lnr-sync'        ,'name' => 'แจ้งเปลี่ยนแปลงห้อง'],
        'payment' => ['role' => 'admin|member', 'route'=> route('payment.index')  , 'icon' => 'lnr-alarm'       ,'name' => 'การชำระเงิน'],
        'history' => ['role' => 'admin|member', 'route'=> route('history.index')  , 'icon' => 'lnr-book'        ,'name' => 'ประวัติการชำระเงิน'],
    ];
}

function month($date)
{
    $month = Carbon::parse($date)->month;
    $year  = Carbon::parse($date)->year;
    $thai_month = ["1"=>"มกราคม ", "2"=>"กุมภาพันธ์ ", "3"=>"มีนาคม ", "4"=>"เมษายน ","5"=>"พฤษภาคม " , "6"=>"มิถุนายน ", "7"=>"กรกฎาคม ","8"=>"สิงหาคม " ,"9"=>"กันยายน ","10"=>"ตุลาคม ","11"=>"พฤศจิกายน ","12"=>"ธันวาคม "];
    return $thai_month[$month]." ปี ".($year + 543);
}

function date_th()
{
    $thai_month = ["1"=>"มกราคม ", "2"=>"กุมภาพันธ์ ", "3"=>"มีนาคม ", "4"=>"เมษายน ","5"=>"พฤษภาคม " , "6"=>"มิถุนายน ", "7"=>"กรกฎาคม ","8"=>"สิงหาคม " ,"9"=>"กันยายน ","10"=>"ตุลาคม ","11"=>"พฤศจิกายน ","12"=>"ธันวาคม "];
    $d = date('d');
    $y = date('Y') + 543;
    return "วันที่ ".$d." เดือน ".$thai_month[date("n")]." ปี ".$y;
}


function datetime_th($date,$time)
{
    $thai_month = ["1"=>"มกราคม ", "2"=>"กุมภาพันธ์ ", "3"=>"มีนาคม ", "4"=>"เมษายน ","5"=>"พฤษภาคม " , "6"=>"มิถุนายน ", "7"=>"กรกฎาคม ","8"=>"สิงหาคม " ,"9"=>"กันยายน ","10"=>"ตุลาคม ","11"=>"พฤศจิกายน ","12"=>"ธันวาคม "];
    $d = Carbon::parse($date)->day;
    $m = Carbon::parse($date)->month;
    $y = Carbon::parse($date)->year + 543;
    return $d." ".$thai_month[$m]." ".$y." || เวลา ".Carbon::parse($time)->format('H:i');
}

function dt($date,$time)
{
    $d = Carbon::parse($date)->day;
    $m = Carbon::parse($date)->month;
    $y = Carbon::parse($date)->year + 543;
    return $d."/".$m."/".$y." || ".Carbon::parse($time)->format('H:i');
}

function dt_th($dt)
{
    $y = Carbon::parse($dt)->format('Y')+543;
    return Carbon::parse($dt)->format('H:i')." || ".Carbon::parse($dt)->format('d/m/').$y;
}

// Admin Change
function change()
{
    $change = Change::where('read', 0)->get();
    $count  = Change::where('read', 0)->count();
    return [
        'change' => $change,
        'count'  => $count
    ];

}

// Admin Alert
function repair()
{
    $repair = Repair::where('read', 0)->get();
    $count  = Repair::where('read', 0)->count();
    return [
        'repair' => $repair,
        'count'  => $count
    ];

}

// Member Alert
function comfirm()
{
    $comfirm = Repair::where('user_id', Auth::user()->id)->where('status', 1)->get();
    $count   = Repair::where('user_id', Auth::user()->id)->where('status', 1)->count();
    return [
        'comfirm' => $comfirm,
        'count'   => $count
    ];
}

// Admin Alert
function payment()
{
    $payment = Detail::where('read', 0)->get();
    $count   = Detail::where('read', 0)->count();
    return [
        'payment' => $payment,
        'count'   => $count
    ];
}

// Member Alert
function accept()
{
    $accept = Payment::where('user_id', Auth::user()->id)->where('status', 0)->get();
    $count  = Payment::where('user_id', Auth::user()->id)->where('status', 0)->count();
    return [
        'accept' => $accept,
        'count'  => $count
    ];
}

// Member Alert
function not()
{
    $not   = Payment::where('user_id', Auth::user()->id)->where('status', 3)->get();
    $count = Payment::where('user_id', Auth::user()->id)->where('status', 3)->count();
    return [
        'not'   => $not,
        'count' => $count
    ];
}

function Numberthai($amount_number)
{
    $amount_number = number_format($amount_number, 2, ".","");
    $pt = strpos($amount_number , ".");
    $number = $fraction = "";
    if ($pt === false)
        $number = $amount_number;
    else
    {
        $number   = substr($amount_number, 0, $pt);
        $fraction = substr($amount_number, $pt + 1);
    }

    $ret = "";
    $baht = ReadNumber($number);
    if ($baht != "")
        $ret .= $baht . "บาท";

    $satang = ReadNumber($fraction);
    if ($satang != "")
        $ret .=  $satang . "สตางค์";
    else
        $ret .= "ถ้วน";
    return $ret;
}

function ReadNumber($number)
{
    $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
    $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
    $number = $number + 0;
    $ret = "";
    if ($number == 0) return $ret;
    if ($number > 1000000)
    {
        $ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
        $number = intval(fmod($number, 1000000));
    }

    $divider = 100000;
    $pos = 0;
    while($number > 0)
    {
        $d = intval($number / $divider);
        $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
            ((($divider == 10) && ($d == 1)) ? "" :
            ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
        $ret .= ($d ? $position_call[$pos] : "");
        $number = $number % $divider;
        $divider = $divider / 10;
        $pos++;
    }
    return $ret;
}

function Color()
{
    return [
            'rgba(54, 162, 235, 0.2)'  , 'rgba(255, 99, 132, 0.2)' , 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)' , 'rgba(230, 126, 34, 0.2)' , 'rgba(44, 62, 80, 0.2)'  , 'rgba(255, 159, 64, 0.2)',
            'rgba(155, 89, 182, 0.2)'  , 'rgba(22, 160, 133, 0.2)' , 'rgba(211, 84, 0, 0.2)'  , 'rgba(52, 73, 94, 0.2)'
        ];
}

function Border()
{
    return [
            'rgba(54, 162, 235, 1)'  , 'rgba(255, 99, 132, 1)' , 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)' , 'rgba(230, 126, 34, 1)' , 'rgba(44, 62, 80, 1)'  , 'rgba(255, 159, 64, 1)',
            'rgba(155, 89, 182, 1)'  , 'rgba(22, 160, 133, 1)' , 'rgba(211, 84, 0, 1)'  , 'rgba(52, 73, 94, 1)'
        ];
}

function M()
{
    $month = ["1"=>" ม.ค.","2"=>" ก.พ.","3"=>" มี.ค.","4"=>" เม.ย.","5"=>" พ.ค.", "6"=>" มิ.ย.", "7"=>" ก.ค.","8"=>" ส.ค.","9"=>" ก.ย.","10"=>" ต.ค.","11"=>" พ.ย.","12"=>" ธ.ค."];
    return $month;
}

?>
