<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $primaryKey = 'payment_id';

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }
}
