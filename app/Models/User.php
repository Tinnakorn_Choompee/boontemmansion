<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'card_id', 'address','username', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static function boot() {
        parent::boot();
        static::deleting(function($user) {
             $user->repair()->delete();
        });
    }

    public function getFullNameAttribute() {
        return $this->prefix. ' ' .$this->first_name. ' ' .$this->last_name;
    }

    public function room()
    {
        return $this->hasOne(Room::class);
    }

    public function repair()
    {
        return $this->hasMany(Repair::class);
    }
}
